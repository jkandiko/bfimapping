﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BFI.Entities.Migrations
{
    public partial class incomingupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "IncomingLocationIssues",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IncomingLocationIssueId",
                table: "IncomingLocationIssueAttachments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_IncomingLocationIssueAttachments_IncomingLocationIssueId",
                table: "IncomingLocationIssueAttachments",
                column: "IncomingLocationIssueId");

            migrationBuilder.AddForeignKey(
                name: "FK_IncomingLocationIssueAttachments_IncomingLocationIssues_IncomingLocationIssueId",
                table: "IncomingLocationIssueAttachments",
                column: "IncomingLocationIssueId",
                principalTable: "IncomingLocationIssues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IncomingLocationIssueAttachments_IncomingLocationIssues_IncomingLocationIssueId",
                table: "IncomingLocationIssueAttachments");

            migrationBuilder.DropIndex(
                name: "IX_IncomingLocationIssueAttachments_IncomingLocationIssueId",
                table: "IncomingLocationIssueAttachments");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "IncomingLocationIssues");

            migrationBuilder.DropColumn(
                name: "IncomingLocationIssueId",
                table: "IncomingLocationIssueAttachments");
        }
    }
}
