﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BFI.Entities.Migrations
{
    public partial class IncomingLocationIssues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IncomingLocationIssueId",
                table: "LocationIssueAttachments",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "IncomingLocationIssueAttachments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Uri = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncomingLocationIssueAttachments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IncomingLocationIssues",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncomingLocationIssues", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LocationIssueAttachments_IncomingLocationIssueId",
                table: "LocationIssueAttachments",
                column: "IncomingLocationIssueId");

            migrationBuilder.AddForeignKey(
                name: "FK_LocationIssueAttachments_IncomingLocationIssues_IncomingLocationIssueId",
                table: "LocationIssueAttachments",
                column: "IncomingLocationIssueId",
                principalTable: "IncomingLocationIssues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LocationIssueAttachments_IncomingLocationIssues_IncomingLocationIssueId",
                table: "LocationIssueAttachments");

            migrationBuilder.DropTable(
                name: "IncomingLocationIssueAttachments");

            migrationBuilder.DropTable(
                name: "IncomingLocationIssues");

            migrationBuilder.DropIndex(
                name: "IX_LocationIssueAttachments_IncomingLocationIssueId",
                table: "LocationIssueAttachments");

            migrationBuilder.DropColumn(
                name: "IncomingLocationIssueId",
                table: "LocationIssueAttachments");
        }
    }
}
