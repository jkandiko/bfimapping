﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public class Country {
        public int Id { get; set; }
        public string IsoCode { get; set; }
        public string Name { get; set; }
        public string Currency { get; set; }
        public string TwoCharCode { get; set; }
    }
}
