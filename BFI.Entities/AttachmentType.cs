﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BFI.Entities {
    public enum AttachmentType {
        [Display(Name = "Image")]
        Image,
        [Display(Name = "Pdf")]
        Pdf
    }
}
