﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public class RegionLocation {
        public int RegionId { get; set; }
        public Region Region { get; set; }
        public int LocationId { get; set; }
        public Location Location { get; set; }
    }
}
