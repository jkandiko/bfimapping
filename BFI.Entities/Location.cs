﻿using BFI.Core.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace BFI.Entities {
    /// <summary>
    /// The central class for identifying a place to find a fix station.
    /// </summary>
    public class Location {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; } = new DateTime();
        public DateTime ModifiedOn { get; set; } = new DateTime();
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        [NotZeroAttribute]
        public decimal Latitude { get; set; }
        [NotZeroAttribute]
        public decimal Longitude { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        [Display(Name = "State / Province")]
        public string StateProvince { get; set; }
        [Display(Name = "Post Code")]
        public string PostCode { get; set; }
        [Required]
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public LocationType Type { get; set; }
        public virtual ICollection<LocationIssue> Issues { get; set; }
        public int CompanyId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public Company Company { get; set; }
        public virtual ICollection<RegionLocation> RegionLocations { get; set; } = new List<RegionLocation>();
        public virtual ICollection<LocationAttachment> Attachments { get; set; }
        public string PinUri { get; set; }
        [Required]
        public string Excerpt { get; set; }
        [Required]
        public string Description { get; set; }
    }
}
