﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public class LocationAttachment : Attachment{
        public int LocationId { get; set; }
        public Location Location { get; set; }
    }
}
