﻿using BFI.Core.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace BFI.Entities {
    public static class DbInitializer {
        public static void Initialize(IServiceProvider serviceProvider) {
            using (var context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>())) {
                if (!context.Roles.Any()) {
                    SeedRoles(context);
                }

                if (!context.RoleClaims.Any()) {
                    SeedRoleClaims(context);
                }

                if (!context.Countries.Any()) {
                    SeedCountries(context);
                }

                if (!context.Companies.Any()) {
                    SeedCompanies(context);
                }
            }
        }

        private static void SeedRoles(ApplicationDbContext context) {
            context.Roles.Add(new Microsoft.AspNetCore.Identity.IdentityRole<long>() {
                Name = "BFI Administrator",
                NormalizedName = "BFI ADMINISTRATOR"
            });

            context.Roles.Add(new Microsoft.AspNetCore.Identity.IdentityRole<long>() {
                Name = "Company Administrator",
                NormalizedName = "COMPANY ADMINISTRATOR"
            });
            context.Roles.Add(new Microsoft.AspNetCore.Identity.IdentityRole<long>() {
                Name = "Company User",
                NormalizedName = "COMPANY USER"
            });

            context.SaveChanges();
        }

        private static void SeedRoleClaims(ApplicationDbContext context) {
            var admin = context.Roles.Where(f => f.Name == "BFI Administrator").First();
            context.RoleClaims.Add(new Microsoft.AspNetCore.Identity.IdentityRoleClaim<long>() { RoleId = admin.Id, ClaimType = "permission", ClaimValue = PermissionNames.CompaniesCreate });
            context.RoleClaims.Add(new Microsoft.AspNetCore.Identity.IdentityRoleClaim<long>() { RoleId = admin.Id, ClaimType = "permission", ClaimValue = PermissionNames.CompaniesUpdate });
            context.RoleClaims.Add(new Microsoft.AspNetCore.Identity.IdentityRoleClaim<long>() { RoleId = admin.Id, ClaimType = "permission", ClaimValue = PermissionNames.CompaniesView });
            context.RoleClaims.Add(new Microsoft.AspNetCore.Identity.IdentityRoleClaim<long>() { RoleId = admin.Id, ClaimType = "permission", ClaimValue = PermissionNames.CompaniesDelete });
            context.SaveChanges();
        }

        private static void SeedCountries(ApplicationDbContext context) {
            context.Countries.Add(new Country() { IsoCode = "USA", Name = "United States", Currency = "USD", TwoCharCode = "US" });
            context.Countries.Add(new Country() { IsoCode = "CAN", Name = "Canada", Currency = "CAD", TwoCharCode = "CA" });
            context.Countries.Add(new Country() { IsoCode = "GBR", Name = "United Kingdom", Currency = "GBP", TwoCharCode = "GB" });
            context.Countries.Add(new Country() { IsoCode = "AUT", Name = "Austria", Currency = "EUR", TwoCharCode = "AT" });
            context.Countries.Add(new Country() { IsoCode = "ITA", Name = "Italy", Currency = "EUR", TwoCharCode = "IT" });
            context.Countries.Add(new Country() { IsoCode = "BAM", Name = "Bosnia", Currency = "BAM", TwoCharCode = "BA" });
            context.Countries.Add(new Country() { IsoCode = "BEL", Name = "Belgium", Currency = "EUR", TwoCharCode = "BE" });
            context.Countries.Add(new Country() { IsoCode = "BGR", Name = "Bulgaria", Currency = "EUR", TwoCharCode = "BG" });
            context.Countries.Add(new Country() { IsoCode = "CIS", Name = "Canary Islands", Currency = "EUR", TwoCharCode = "CI" });
            context.Countries.Add(new Country() { IsoCode = "HRV", Name = "Croatia", Currency = "EUR", TwoCharCode = "HR" });
            context.Countries.Add(new Country() { IsoCode = "CZE", Name = "Czech Republic", Currency = "EUR", TwoCharCode = "CZ" });
            context.Countries.Add(new Country() { IsoCode = "DNK", Name = "Denmark", Currency = "EUR", TwoCharCode = "DK" });
            context.Countries.Add(new Country() { IsoCode = "EST", Name = "Estonia", Currency = "EUR", TwoCharCode = "EE" });
            context.Countries.Add(new Country() { IsoCode = "FIN", Name = "Finland", Currency = "EUR", TwoCharCode = "FI" });
            context.Countries.Add(new Country() { IsoCode = "FRA", Name = "France", Currency = "EUR", TwoCharCode = "FR" });
            context.Countries.Add(new Country() { IsoCode = "DEU", Name = "Germany", Currency = "USD", TwoCharCode = "DE" });
            context.Countries.Add(new Country() { IsoCode = "GRC", Name = "Greece", Currency = "EUR", TwoCharCode = "GR" });
            context.Countries.Add(new Country() { IsoCode = "HUN", Name = "Hungary", Currency = "EUR", TwoCharCode = "HU" });
            context.Countries.Add(new Country() { IsoCode = "ISL", Name = "Iceland", Currency = "ISK", TwoCharCode = "IS" });
            context.Countries.Add(new Country() { IsoCode = "LVA", Name = "Latvia", Currency = "EUR", TwoCharCode = "LV" });
            context.Countries.Add(new Country() { IsoCode = "LTU", Name = "Lithuania", Currency = "EUR", TwoCharCode = "LT" });
            context.Countries.Add(new Country() { IsoCode = "LUX", Name = "Luxembourg", Currency = "EUR", TwoCharCode = "LU" });
            context.Countries.Add(new Country() { IsoCode = "NLD", Name = "Netherlands", Currency = "EUR", TwoCharCode = "NL" });
            context.Countries.Add(new Country() { IsoCode = "NOR", Name = "Norway", Currency = "NOK", TwoCharCode = "NO" });
            context.Countries.Add(new Country() { IsoCode = "POL", Name = "Poland", Currency = "EUR", TwoCharCode = "PL" });
            context.Countries.Add(new Country() { IsoCode = "PRT", Name = "Portugal", Currency = "EUR", TwoCharCode = "PT" });
            context.Countries.Add(new Country() { IsoCode = "ROU", Name = "Romania", Currency = "EUR", TwoCharCode = "RO" });
            context.Countries.Add(new Country() { IsoCode = "SRB", Name = "Serbia", Currency = "RSD", TwoCharCode = "RS" });
            context.Countries.Add(new Country() { IsoCode = "SVK", Name = "Slovakia", Currency = "EUR", TwoCharCode = "SK" });
            context.Countries.Add(new Country() { IsoCode = "SVN", Name = "Slovenia", Currency = "EUR", TwoCharCode = "SI" });
            context.Countries.Add(new Country() { IsoCode = "ESP", Name = "Spain", Currency = "EUR", TwoCharCode = "ES" });
            context.Countries.Add(new Country() { IsoCode = "SWE", Name = "Sweden", Currency = "EUR", TwoCharCode = "SE" });
            context.Countries.Add(new Country() { IsoCode = "CHE", Name = "Switzerland", Currency = "CHF", TwoCharCode = "CH" });
            context.SaveChanges();
        }

        private static void SeedCompanies(ApplicationDbContext context) {
            context.Companies.Add(new Company() { Name = "DOT - Yale", CreatedBy = "DATASEED", CreatedOn = DateTime.Now, ModifiedBy = "DATASEED", ModifiedOn = DateTime.Now });

            context.SaveChanges();

            var yale = context.Companies.First();

            context.Locations.Add(new Location() {
                Address = "720 Edgewood Ave",
                City = "New Haven",
                StateProvince = "CT",
                PostCode = "06515",
                CompanyId = yale.Id,
                CountryId = 1,
                CreatedBy = "DATASEED",
                CreatedOn = DateTime.Now,
                ModifiedBy = "DATASEED",
                ModifiedOn = DateTime.Now,
                Name = "Bike Repair Station",
                Excerpt = "Short Excerpt",
                Description = "Longer description",
                Latitude = 41.315745M,
                Longitude = -72.958074M,
                PinUri = "https://maps.google.com/mapfiles/kml/shapes/library_maps.png"
            });

            context.Locations.Add(new Location() {
                Address = "409 Prospect St",
                City = "New Haven",
                StateProvince = "CT",
                PostCode = "06511",
                CompanyId = yale.Id,
                CountryId = 1,
                CreatedBy = "DATASEED",
                CreatedOn = DateTime.Now,
                ModifiedBy = "DATASEED",
                ModifiedOn = DateTime.Now,
                Name = "Yale Divinity School Repair Station",
                Excerpt = "Short Excerpt",
                Description = "Longer description",
                Latitude = 41.323159M,
                Longitude = -72.9220179M,
            });

            context.Locations.Add(new Location() {
                Address = "50 Union Ave",
                City = "New Haven",
                StateProvince = "CT",
                PostCode = "06519",
                CompanyId = yale.Id,
                CountryId = 1,
                CreatedBy = "DATASEED",
                CreatedOn = DateTime.Now,
                ModifiedBy = "DATASEED",
                ModifiedOn = DateTime.Now,
                Name = "Union Station Repair Station",
                Excerpt = "Short Excerpt",
                Description = "Longer description",
                Latitude = 41.2976686M,
                Longitude = -72.9257885M,
                PinUri = "https://maps.google.com/mapfiles/kml/shapes/info-i_maps.png"
            });

            context.SaveChanges();

            context.LocationAttachments.Add(new LocationAttachment() {
                Name = "2mrw5g4.jpg",
                Description = "White pump station",
                CreatedBy = "DATASEED",
                CreatedOn = DateTime.Now,
                ModifiedBy = "DATASEED",
                ModifiedOn = DateTime.Now,
                Type = AttachmentType.Image,
                Uri = "/locationattachments/1.jpg",
                LocationId = context.Locations.First().Id
            });

            context.SaveChanges();

            

            context.Regions.Add(new Region() { ModifiedBy = "DATASEED", ModifiedOn = DateTime.Now, CreatedBy = "DATASEED", CreatedOn = DateTime.Now, Name = "Yale University", CenterLatitude = 41.3163244M, CenterLongitude = -72.922343M, Uri= "YaleUniversity", InitialZoom = 14 });

            context.SaveChanges();

            var universityLocation = context.Regions.First();

            foreach (var location in context.Locations) {
                universityLocation.RegionLocations.Add(new RegionLocation() { LocationId = location.Id, RegionId = universityLocation.Id });
            }
            context.SaveChanges();
        }
    }
}
