﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public class CompanyUser {
        
        public int CompanyId { get; set; }
        public Company Company { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
    }
}
