﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace BFI.Entities {
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class User : IdentityUser<long> {
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<CompanyUser> Companies { get; set; }
    }
}
