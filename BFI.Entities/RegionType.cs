﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public enum RegionType {
        City,
        University,
        Airport
    }
}
