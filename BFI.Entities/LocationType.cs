﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BFI.Entities {
    public enum LocationType {
        [Display(Name = "Repair Station")]
        RepairStation,
        [Display(Name = "Bike Pump Station")]
        BikePumpStation,
        [Display(Name = "Bike Rack Station")]
        BikeRackStation,
        [Display(Name = "Bottle Fill Station")]
        BottleFillStation
    }
}
