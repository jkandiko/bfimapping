﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public class LocationIssueAttachment : Attachment {
        public int LocationIssueId { get; set; }
        public LocationIssue LocationIssue { get; set; }
    }
}
