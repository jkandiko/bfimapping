﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public enum IssueStatus {
        Pending,
        Active,
        Resolved
    }
}
