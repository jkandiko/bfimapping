﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public class IncomingLocationIssueAttachment : Attachment {
        public int IncomingLocationIssueId { get; set; }
        public IncomingLocationIssue IncomingLocationIssue { get; set; }
    }
}
