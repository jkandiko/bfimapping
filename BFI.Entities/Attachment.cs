﻿using System;

namespace BFI.Entities {
    public abstract class Attachment {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public DateTime ModifiedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public AttachmentType Type { get; set; }
        public string Description { get; set; }
        public string Uri { get; set; }
    }
}
