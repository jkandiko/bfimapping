﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BFI.Entities {
    public class ApplicationDbContext : IdentityDbContext<
            User,
            IdentityRole<long>,
            long,
            IdentityUserClaim<long>,
            IdentityUserRole<long>,
            IdentityUserLogin<long>,
            IdentityRoleClaim<long>,
            IdentityUserToken<long>> {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder) {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<User>().ToTable("User");
            builder.Entity<IdentityRole<long>>().ToTable("Role");
            builder.Entity<IdentityUserClaim<long>>().ToTable("UserClaim");
            builder.Entity<IdentityUserLogin<long>>().ToTable("UserLogin");
            builder.Entity<IdentityUserRole<long>>().ToTable("UserRole");
            builder.Entity<IdentityUserToken<long>>().ToTable("UserToken");
            builder.Entity<IdentityRoleClaim<long>>().ToTable("RoleClaim");

            builder.Entity<RegionLocation>().HasKey(t => new { t.RegionId, t.LocationId });
            builder.Entity<CompanyUser>().HasKey(t => new { t.CompanyId, t.UserId });

            foreach (var property in builder.Model.GetEntityTypes().SelectMany(t => t.GetProperties()).Where(p => p.ClrType == typeof(decimal))) {
                property.Relational().ColumnType = "decimal(18, 16)";
            }
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<LocationIssue> LocationIssues { get; set; }
        public DbSet<LocationIssueAttachment> LocationIssueAttachments { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<LocationAttachment> LocationAttachments { get; set; }
        public DbSet<IncomingLocationIssue> IncomingLocationIssues { get; set; }
        public DbSet<IncomingLocationIssueAttachment> IncomingLocationIssueAttachments { get; set; }
    }
}
