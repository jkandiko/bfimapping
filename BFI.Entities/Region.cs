﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public class Region {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public DateTime ModifiedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public decimal CenterLatitude { get; set; }
        public decimal CenterLongitude { get; set; }
        public decimal InitialZoom { get; set; }
        public RegionType Type { get; set; }
        public virtual ICollection<RegionLocation> RegionLocations { get; set; } = new List<RegionLocation>();
        public string Uri { get; set; }
    }
}
