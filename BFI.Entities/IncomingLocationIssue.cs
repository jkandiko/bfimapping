﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Entities {
    public class IncomingLocationIssue {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public DateTime ModifiedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public virtual ICollection<LocationIssueAttachment> Attachments { get; set; }
        public string Description { get; set; }
    }
}
