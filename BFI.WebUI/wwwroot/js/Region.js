﻿var map;
var infoWindow;

function initMap() {
    infowindow = new google.maps.InfoWindow();

    $.post(apiUrl + 'Region/Search',
        { uri: regionUri },
        function (data, textSTatus, jqXHR) {
            map = new google.maps.Map(document.getElementById('map'), {
                center: { lat: data.region.centerLatitude, lng: data.region.centerLongitude},
                zoom: data.region.initialZoom
            });

            loadLocationData(map, data.locations);
        }
    );
}