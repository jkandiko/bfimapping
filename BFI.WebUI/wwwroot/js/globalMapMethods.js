﻿function reportIssueDialog(id) {
    $("#issueReport").dialog({
        autoOpen: false,
        show: { effect: "blind", duration: 250 },
        hide: { effect: "blind", duration: 250 },
        resizable: false,
        height: "auto",
        modal: true,
    })
    $("#issueReport").dialog("open").data('locationId', id);
}

function logIssue(id, description) {
    $.post(apiUrl + 'Issue/Log',
        { Id: id, Description: description },
        function (data, textStatus, jqXHR) {

        }
    );
}

function loadLocationData(map, locations) {
    var marker, i;
    var contentStrings = [];

    for (i = 0; i < locations.length; i++) {
        var location = locations[i].location ? locations[i].location : locations[i];
        contentStrings.push(locations[i].contentString);

        marker = new google.maps.Marker({
            position: { lat: location.latitude, lng: location.longitude },
            map: map,
            title: location.name,
            icon: location.pinUri !== '' ? location.pinUri : null
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(contentStrings[i]);
                infowindow.open(map, marker);
            };
        })(marker, i));
    }
}

function submitIssue(dialog, description) {
    logIssue(dialog.data('locationId'), description.val());
    description.val('');
    dialog.dialog("close");
}

function cancelIssue(dialog, description) {
    description.val('');
    dialog.dialog("close");
}

