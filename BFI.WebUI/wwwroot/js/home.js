﻿var map;
var infoWindow;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: new google.maps.LatLng(44, -110),
    });

    infowindow = new google.maps.InfoWindow();

    google.maps.event.addListener(map, 'dragend', function () {
        var newBounds = map.getBounds();
        if (newBounds) {
            loadPoints(
                newBounds.getSouthWest().lat(),
                newBounds.getSouthWest().lng(),
                newBounds.getNorthEast().lat(),
                newBounds.getNorthEast().lng()
            );
        }
    });
    

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(pos);

            var newBounds = map.getBounds();
            if (newBounds) {
                loadPoints(
                    newBounds.getSouthWest().lat(),
                    newBounds.getSouthWest().lng(),
                    newBounds.getNorthEast().lat(),
                    newBounds.getNorthEast().lng()
                );
            }
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function loadPoints(swLat, swLon, neLat, neLon) {
    $.post(apiUrl + 'Home/Search', {
            southwestLatitude: swLat,
            southwestLongitude: swLon,
            northeastLatitude: neLat,
            northeastLongitude: neLon
        }, function (data, textSTatus, jqXHR) {
            loadLocationData(map, data);
            }
        );
}
