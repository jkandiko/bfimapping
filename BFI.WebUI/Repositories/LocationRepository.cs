﻿using BFI.Core.Models;
using BFI.Entities;
using BFI.WebUI.Models;
using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BFI.WebUI.Repositories {
    public class LocationRepository : BaseRepository {
        private readonly ILogger _logger;
        private IConfiguration configuration;

        public LocationRepository(ILogger<LocationRepository> logger, IConfiguration configuration) {
            _logger = logger;
            this.configuration = configuration;
        }

        public async Task<List<Location>> Search(string accessToken, MapParams options) {
            var ret = new List<Location>();

            var client = base.SetUpApiClient(accessToken, configuration["bfiApiUri"]);
            var content = new StringContent(JsonConvert.SerializeObject(options), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("Location/Search", content);
            if(response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Location>>();
            }

            return ret;
        }

        public async Task<bool> LogIssue(string accessToken, Issue model) {
            var client = base.SetUpApiClient(accessToken, configuration["bfiApiUri"]);
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("Location/LogIssue", content);
            if(response.IsSuccessStatusCode) {
                return true;
            }

            return false;
        }
    }
}
