﻿using BFI.Core.Models;
using BFI.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BFI.WebUI.Repositories
{
    public class RegionRepository : BaseRepository {
        private readonly ILogger _logger;
        private IConfiguration configuration;

        public RegionRepository(ILogger<LocationRepository> logger, IConfiguration configuration) {
            _logger = logger;
            this.configuration = configuration;
        }

        public async Task<Region> Search(string accessToken, MapParams options) {
            var ret = new Region();

            var client = base.SetUpApiClient(accessToken, configuration["bfiApiUri"]);
            var content = new StringContent(JsonConvert.SerializeObject(options), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("Region/GetByUri", content);
            if(response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Region>();
            }

            return ret;
        }
    }
}