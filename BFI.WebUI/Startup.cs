﻿using BFI.Core.Security;
using BFI.WebUI.Repositories;
using BFI.WebUI.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using static BFI.WebUI.Services.ViewRenderService;

namespace BFI.WebUI {
    public class Startup {
        public IHostingEnvironment HostingEnvironment { get; }
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) {
            var builder = new ConfigurationBuilder();

            builder.AddUserSecrets<Startup>();  // https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?tabs=visual-studio-code

            Configuration = builder.Build();

            foreach (var item in configuration.AsEnumerable()) {
                Configuration[item.Key] = item.Value;
            }
        }

        public Startup(IHostingEnvironment env) {
            var builder = new ConfigurationBuilder();

            if (env.IsDevelopment()) {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }

        public Startup(IHostingEnvironment env, IConfiguration config) {
            HostingEnvironment = env;
            Configuration = config;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc();

            services.AddAuthorization(options => {
                foreach(var item in Policies.Build()) {
                    options.AddPolicy(item.Key, item.Value);
                }
            });

            services.AddScoped<IViewRenderService, ViewRenderService>();
            services.AddMemoryCache();
            services.AddTransient<LocationRepository, LocationRepository>();
            services.AddTransient<RegionRepository, RegionRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                
            } else {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
