﻿using BFI.Core.Constants;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebUI.Controllers
{
    public abstract class BaseController : Controller {
        public string LocationAttachmentUri {
            get {
                return Configuration["attachmentLocationUri"];
            }
        }

        public IConfiguration Configuration { get; set; }
        public IMemoryCache MemoryCache { get; set; }

        public BaseController(IConfiguration configuration, IMemoryCache memoryCache) {
            this.Configuration = configuration;
            this.MemoryCache = memoryCache;
        }

        public async Task<string> GetAccessToken() {
            string result;
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(60));

            if(!MemoryCache.TryGetValue("ACCESSTOKEN", out result)) {
                var disco = await DiscoveryClient.GetAsync(Configuration["IdentityServerAddress"]);
                // request token
                var tokenClient = new TokenClient(disco.TokenEndpoint, Configuration["clientId"], Configuration["clientSecret"]);
                var tokenResponse = await tokenClient.RequestClientCredentialsAsync(Configuration["apiName"]);
                result = tokenResponse.AccessToken;
                MemoryCache.Set("ACCESSTOKEN", result, cacheEntryOptions);
            }
            var accessToken = result;

            var jwthandler = new JwtSecurityTokenHandler();
            var jwttoken = jwthandler.ReadToken(accessToken);
            var expDate = jwttoken.ValidTo;
            if(expDate < DateTime.UtcNow.AddMinutes(1)) {
                var disco = await DiscoveryClient.GetAsync(Configuration["IdentityServerAddress"]);
                if(disco.IsError)
                    throw new Exception(disco.Error);

                var tokenClient = new TokenClient(disco.TokenEndpoint, Configuration["clientId"], Configuration["clientSecret"]);
                var rt = await HttpContext.GetTokenAsync("refresh_token");
                var tokenResult = await tokenClient.RequestRefreshTokenAsync(rt);

                if(!tokenResult.IsError) {
                    var old_id_token = await HttpContext.GetTokenAsync("id_token");
                    var new_access_token = tokenResult.AccessToken;
                    var new_refresh_token = tokenResult.RefreshToken;

                    var tokens = new List<AuthenticationToken>();
                    tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.IdToken, Value = old_id_token });
                    tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.AccessToken, Value = new_access_token });
                    tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.RefreshToken, Value = new_refresh_token });

                    var expiresAt = DateTime.UtcNow + TimeSpan.FromSeconds(tokenResult.ExpiresIn);
                    tokens.Add(new AuthenticationToken { Name = "expires_at", Value = expiresAt.ToString("o", CultureInfo.InvariantCulture) });

                    var info = await HttpContext.AuthenticateAsync("Cookies");
                    info.Properties.StoreTokens(tokens);
                    await HttpContext.SignInAsync("Cookies", info.Principal, info.Properties);



                }
                accessToken = GetAccessToken().Result;
            }

            return accessToken;
        }
    }
}
