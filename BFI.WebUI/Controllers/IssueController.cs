﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BFI.Core.Models;
using BFI.WebUI.Models;
using BFI.WebUI.Repositories;
using BFI.WebUI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace BFI.WebUI.Controllers
{
    public class IssueController : BaseController {
        private LocationRepository LocationRepo { get; set; }

        public IssueController(IConfiguration configuration, LocationRepository locationRepository, IMemoryCache cache) : base(configuration, cache) {
            Configuration = configuration;
            LocationRepo = locationRepository;
        }

        public async Task<IActionResult> Log(Issue model) {
            string accessToken = await base.GetAccessToken();
            var result = await LocationRepo.LogIssue(accessToken, model);

           
            return new JsonResult(result);

        }
    }
}
