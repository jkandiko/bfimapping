﻿using BFI.Core.Constants;
using BFI.Core.Models;
using BFI.WebUI.Models;
using BFI.WebUI.Repositories;
using BFI.WebUI.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BFI.WebUI.Controllers {
    public class HomeController : BaseController {
        private LocationRepository LocationRepo { get; set; }
        private IViewRenderService RenderService { get; set; }

        public HomeController(IConfiguration configuration, LocationRepository locationRepository, IViewRenderService viewRenderService, IMemoryCache cache) :base(configuration, cache) {
            Configuration = configuration;
            LocationRepo = locationRepository;
            RenderService = viewRenderService;
        }

        public IActionResult Index() {
            ViewData["bfiApiUri"] = Configuration["bfiApiUri"];
            ViewData["webAppUri"] = Configuration["webAppUri"];
            ViewData["googleMapsApiKey"] = Configuration["GoogleMapsApiKey"];
            ViewData["attachmentUri"] = Configuration["attachmentUri"];
            return View();
        }

        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Search(decimal southwestLatitude, decimal southwestLongitude, decimal northeastLatitude, decimal northeastLongitude) {
            var options = new MapParams();
            options.SouthwestLatitude = southwestLatitude;
            options.SouthwestLongitude = southwestLongitude;
            options.NortheastLatitude = northeastLatitude;
            options.NortheastLongitude = northeastLongitude;

            string accessToken = await base.GetAccessToken();
            var locations = await LocationRepo.Search(accessToken, options);

            var viewModels = new List<LocationViewModel>();
            foreach(var item in locations) {
                var newVM = new LocationViewModel();
                newVM.Location = item;
                newVM.AttachmentUri = LocationAttachmentUri;
                newVM.ContentString = await RenderService.RenderToStringAsync("Shared/_GoogleMapsInfoWIndow", newVM);
                viewModels.Add(newVM);
            }
            return new JsonResult(viewModels);

        }
    }
}
