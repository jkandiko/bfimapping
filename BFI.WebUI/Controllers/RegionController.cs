﻿using BFI.Core.Models;
using BFI.WebUI.Models;
using BFI.WebUI.Repositories;
using BFI.WebUI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BFI.WebUI.Controllers {
    public class RegionController : BaseController {
        private RegionRepository RegionRepo { get; set; }
        private IViewRenderService RenderService { get; set; }

        public RegionController(IConfiguration configuration, RegionRepository regionRepository, IViewRenderService viewRenderService, IMemoryCache cache) : base(configuration, cache) {
            Configuration = configuration;
            RegionRepo = regionRepository;
            RenderService = viewRenderService;
        }
        public IActionResult Index(string uri) {
            ViewData["bfiApiUri"] = Configuration["bfiApiUri"];
            ViewData["webAppUri"] = Configuration["webAppUri"];
            ViewData["regionUri"] = uri;
            ViewData["googleMapsApiKey"] = Configuration["GoogleMapsApiKey"];
            ViewData["attachmentUri"] = Configuration["attachmentUri"];
            return View();
        }

        public async Task<IActionResult> Search(string uri) {
            var options = new MapParams();
            options.Uri = uri;

            string accessToken = await base.GetAccessToken();
            var region = await RegionRepo.Search(accessToken, options);

            var viewModel = new RegionViewModel();
            viewModel.Locations = new List<LocationViewModel>();
            viewModel.Region = region;
            foreach(var item in region.RegionLocations) {
                var newVM = new LocationViewModel();
                newVM.Location = item.Location;
                newVM.AttachmentUri = LocationAttachmentUri;
                newVM.ContentString = await RenderService.RenderToStringAsync("Shared/_GoogleMapsInfoWIndow", newVM);
                viewModel.Locations.Add(newVM);
            }
            return new JsonResult(viewModel);

        }
    }
}