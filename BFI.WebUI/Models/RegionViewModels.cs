﻿using BFI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebUI.Models {
    public class RegionViewModel{
        public Region Region { get; set; }

        public List<LocationViewModel> Locations { get; set; }
    }
}
