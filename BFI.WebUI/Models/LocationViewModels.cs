﻿using BFI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebUI.Models {
    public abstract class BaseLocationViewModel {
        public string AttachmentUri { get; set; }
    }
    public class LocationViewModel : BaseLocationViewModel {

        public Location Location { get; set; }

        public string ContentString { get; set; }

    }
}
