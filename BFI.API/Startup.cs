﻿using BFI.API.Models;
using BFI.API.Models.IssueSteps;
using BFI.Core.Constants;
using BFI.Core.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BFI.API.Services;
namespace BFI.API {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddDbContext<BFI.Entities.ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services
                .AddMvcCore()           // allow controllers to work
                .AddJsonFormatters();   // without a formatter, nothing would return

            services.AddAuthentication("Bearer")    
                .AddIdentityServerAuthentication(options => {       // allow our identity server to be the bearer of authentication data
                    options.Authority = "http://localhost:5000";
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "BFI_API_V1";
                });

            services.AddAuthorization(options => {
                foreach(var item in Policies.Build()) {
                    options.AddPolicy(item.Key, item.Value);
                }
            });

            services.AddCors(options => {
                options.AddPolicy("PublicGetOnlyPolicy", builder => builder.AllowAnyOrigin().WithMethods("GET"));
                options.AddPolicy("PublicPostOnlyPolicy", builder => builder.AllowAnyOrigin().WithMethods("POST"));
            });

            services.AddSingleton<IAuthorizationHandler, RoleRequirementHandler>();

            services.AddIssueDispatcher();
        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env) {
            app.UseAuthentication();

            app.UseCors("PublicPostOnlyPolicy");

            app.UseMvc();

            app.UseStaticFiles();
        }
    }
}
