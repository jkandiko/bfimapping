﻿using BFI.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps {
    public class CopyToLocationIssues : IDispatchStep {
        private IServiceProvider serviceProvider;

        public CopyToLocationIssues(IServiceProvider serviceProvider) {
            this.serviceProvider = serviceProvider;
        }

        public DispatchResult Execute(int id) {
            try {
                using(IServiceScope scope = serviceProvider.CreateScope()) {
                    var logger = serviceProvider.GetRequiredService<ILogger<CopyToLocationIssues>>();
                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                    logger.LogDebug($"Looking up:{id}");

                    var found = context.IncomingLocationIssues.Where(f => f.Id == id).FirstOrDefault();
                    
                    if(found == null) {
                        logger.LogError($"{id} was not found");
                        return new DispatchResult() { ContinueFurtherDispatches = false, ResultMessage = $"{id} was not found.  Cancelling all further dispatches." };
                    }

                    var location = context.Locations.Include(f=>f.Issues).Where(f => f.Id == found.LocationId).FirstOrDefault();
                    var newIssue = new LocationIssue();
                    newIssue.CreatedBy = found.CreatedBy;
                    newIssue.CreatedOn = found.CreatedOn;
                    newIssue.ModifiedBy = found.ModifiedBy;
                    newIssue.ModifiedOn = found.ModifiedOn;
                    newIssue.Description = found.Description;
                    newIssue.Name = found.Name;
                    newIssue.Status = IssueStatus.Active;
                    location.Issues.Add(newIssue);

                    logger.LogDebug($"Inserting location issue from incoming:{id}");
                    context.SaveChanges();

                    return new DispatchResult() { ContinueFurtherDispatches = true };
                }
            } catch(Exception ex) {
                return new DispatchResult() { ContinueFurtherDispatches = false, ResultMessage = ex.ToString() };
            }
        }
    }
}
