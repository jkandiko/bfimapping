﻿using BFI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps {
    public interface IDispatchStep {
        DispatchResult Execute(int id);
    }
}
