﻿using BFI.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps {
    public class NotifySaris : IDispatchStep {
        private IServiceProvider serviceProvider;

        public NotifySaris(IServiceProvider serviceProvider) {
            this.serviceProvider = serviceProvider;
        }


        public DispatchResult Execute(int id) {
            try {
                var logger = serviceProvider.GetRequiredService<ILogger<NotifyLocationOwner>>();
                logger.LogDebug($"Executing NotifySaris for: {id}");
                logger.LogDebug("if I were a real app, I'd send an email to a list of people inside Saris right now");
                return new DispatchResult() { ContinueFurtherDispatches = true };
            } catch(Exception ex) {
                return new DispatchResult() { ContinueFurtherDispatches = false, ResultMessage = ex.ToString() };
            }
        }
    }
}
