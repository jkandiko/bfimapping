﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps {
    public class IssueDispatchOptions {
        public List<IDispatchStep> DispatchSteps { get; set; }
        public int PollingInterval { get; set; }
    }
}
