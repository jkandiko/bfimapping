﻿using BFI.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps {
    public class RemoveLocationIssue : IDispatchStep {
        private IServiceProvider serviceProvider;

        public DispatchResult Execute(int id) {
            try {
                using(IServiceScope scope = serviceProvider.CreateScope()) {
                    var logger = scope.ServiceProvider.GetRequiredService<ILogger<RemoveLocationIssue>>();
                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                    logger.LogDebug($"Looking up:{id}");

                    var found = context.IncomingLocationIssues.Where(f => f.Id == id).FirstOrDefault();

                    if(found == null) {
                        logger.LogError($"{id} was not found");
                        return new DispatchResult() { ContinueFurtherDispatches = false, ResultMessage = $"{id} was not found.  Cancelling all further dispatches." };
                    }

                    logger.LogDebug($"Removing location issue from incoming:{id}");

                    context.IncomingLocationIssues.Remove(found);
                    context.SaveChanges();

                    return new DispatchResult() { ContinueFurtherDispatches = true };
                }
            } catch(Exception ex) {
                return new DispatchResult() { ContinueFurtherDispatches = false, ResultMessage = ex.ToString() };
            }
        }

        public RemoveLocationIssue(IServiceProvider serviceProvider) {
            this.serviceProvider = serviceProvider;
        }
    }
}
