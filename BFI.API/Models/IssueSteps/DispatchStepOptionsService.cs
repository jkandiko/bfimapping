﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps {
    public interface IDispatchStepOptionsService {
        IssueDispatchOptions Options();
    }
    public class DispatchStepOptionsService : IDispatchStepOptionsService {
        private readonly IServiceProvider serviceProvider;
        public DispatchStepOptionsService(IServiceProvider serviceProvider) {
            this.serviceProvider = serviceProvider;
        }
        public IssueDispatchOptions Options() {
            var options = new IssueDispatchOptions();
            options.PollingInterval = 5000;
            options.DispatchSteps = new List<IDispatchStep>();
            options.DispatchSteps.Add(new CopyToLocationIssues(serviceProvider));
            options.DispatchSteps.Add(new NotifySaris(serviceProvider));
            options.DispatchSteps.Add(new NotifyLocationOwner(serviceProvider));
            options.DispatchSteps.Add(new RemoveLocationIssue(serviceProvider));
            return options;
        }

    }
}
