﻿using BFI.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps {
    public class IssueDispatcher : BackgroundService {
        private ILogger logger;
        private IssueDispatchOptions options;
        private IDispatchStepOptionsService optionsService;

        private readonly IServiceProvider _provider;

        public IssueDispatcher(IServiceProvider serviceProvider, ILogger<IssueDispatcher> logger, IDispatchStepOptionsService optionsService) {
            _provider = serviceProvider;
            this.logger = logger;
            this.optionsService = optionsService;
            options = optionsService.Options();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            logger.LogDebug($"Issue Dispatcher is starting.");

            stoppingToken.Register(() => logger.LogDebug($"Issue Dispatcher background task is stopping."));

            while(!stoppingToken.IsCancellationRequested) {
                using(IServiceScope scope = _provider.CreateScope()) {
                    logger.LogDebug($"Issue Dispatcher task doing background work.");

                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                    
                    if(options != null) {
                        foreach(var issue in context.IncomingLocationIssues) {

                            for(var i = 0; i < options.DispatchSteps.Count; i++) {
                                var ret = options.DispatchSteps[i].Execute(issue.Id);

                                if(!ret.ContinueFurtherDispatches) {
                                    i = options.DispatchSteps.Count;
                                }
                            }
                        }
                    }
                }

                await Task.Delay(options.PollingInterval, stoppingToken);
            }

            logger.LogDebug($"Issue Dispatcher background task is stopping.");
        }
    }
}
