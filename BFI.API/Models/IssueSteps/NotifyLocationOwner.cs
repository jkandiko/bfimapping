﻿using BFI.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps {
    public class NotifyLocationOwner : IDispatchStep {
        private IServiceProvider serviceProvider;

        public NotifyLocationOwner(IServiceProvider serviceProvider) {
            this.serviceProvider = serviceProvider;
        }


        public DispatchResult Execute(int id) {
            try {
                var logger = serviceProvider.GetRequiredService<ILogger<NotifyLocationOwner>>();
                logger.LogDebug($"Executing NotifyLocation Owner for: {id}");
                logger.LogDebug("Notifying the owner of the location about the issue.");
                return new DispatchResult() { ContinueFurtherDispatches = true };
            } catch(Exception ex) {
                return new DispatchResult() { ContinueFurtherDispatches = false, ResultMessage = ex.ToString() };
            }
        }
    }
}
