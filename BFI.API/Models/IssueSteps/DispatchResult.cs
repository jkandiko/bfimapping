﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Models.IssueSteps { 
    public class DispatchResult {
        public bool ContinueFurtherDispatches { get; set; }
        public string ResultMessage { get; set; }

    }
}
