﻿using BFI.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using BFI.Core.Constants;
using Microsoft.Extensions.Logging;
using System;
using BFI.Core.Extensions;
using BFI.Core.Models;

namespace BFI.API.Controllers {
    [Route("api/[controller]")]
    public class RegionController : Controller {

        private readonly ApplicationDbContext context;
        private ILogger logger;
        public RegionController(ApplicationDbContext context, ILogger<RegionController> _logger) {
            this.context = context;
            logger = _logger;
        }
        [HttpGet]
        [Authorize(Policy = PolicyNames.BFIAdmin)]
        public IEnumerable<Region> GetAll() {
            logger.LogDebug("Getting all regions");
            return context.Regions.ToList();
        }

        [EnableCors("PublicPostOnlyPolicy")]
        [Route("getByUri", Name = "GetByUri")]
        [HttpPost]
        public IActionResult GetByUri([FromBody]MapParams options) {
            var item = context.Regions
                .Include(f => f.RegionLocations)
                .ThenInclude(g => g.Location)
                .ThenInclude(g => g.Attachments)
                .Where(f => f.Uri == options.Uri).FirstOrDefault();

            if(item == null) {
                return NotFound();
            }

            foreach(var reg in item.RegionLocations) {
                reg.Region = null;

                foreach(var loc in reg.Location.RegionLocations) {
                    loc.Location.RegionLocations = null;
                }

                foreach(var att in reg.Location.Attachments) {
                    att.Location = null;
                }
            }

            return new ObjectResult(item);
        }


        [HttpGet("{id}", Name = "GetRegion")]
        [Authorize(Policy = PermissionNames.RegionsView)]
        public IActionResult GetById(int id){
            var item = context.Regions
                .Include(f => f.RegionLocations)
                .ThenInclude(g => g.Location)
                .ThenInclude(g => g.Attachments)
                .Where(f => f.Id == id).FirstOrDefault();

            if (item == null) {
                return NotFound();
            }

            foreach(var reg in item.RegionLocations) {
                reg.Region = null;

                foreach(var loc in reg.Location.RegionLocations) {
                    loc.Location.RegionLocations = null;
                }

                foreach(var att in reg.Location.Attachments) {
                    att.Location = null;
                }
            }
            
            return new ObjectResult(item);
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.RegionsCreate)]
        public IActionResult Create([FromBody] Region region) {
            if (region == null) {
                return BadRequest();
            }
            logger.LogDebug("Creating region: " + region.Name);

            region.CreatedOn = DateTime.Now;
            region.ModifiedOn = DateTime.Now;
            region.CreatedBy = User.Name() ?? "API";
            region.ModifiedBy = region.CreatedBy;

            context.Regions.Add(region);
            context.SaveChanges();

            return CreatedAtRoute("GetRegion", new { id = region.Id }, region);
        }

        [HttpPut("{id}")]
        [Authorize(Policy = PermissionNames.RegionsUpdate)]
        public IActionResult Update(long id, [FromBody] Region region) {
            if (region == null) {
                return BadRequest();
            }
            logger.LogDebug("Updating region: " + region.Name);

            var existing = context.Regions.FirstOrDefault(t => t.Id == id);
            if (existing == null) {
                return NotFound();
            }

            existing.Name = region.Name;
            existing.Type = region.Type;
            existing.Uri = region.Uri;
            existing.InitialZoom = region.InitialZoom;
            existing.CenterLatitude = region.CenterLatitude;
            existing.CenterLongitude = region.CenterLongitude;
            existing.ModifiedBy = User.Name() ?? "API";
            existing.ModifiedOn = DateTime.Now;

            context.Update(existing);
            context.SaveChanges();

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = PermissionNames.RegionsDelete)]
        public IActionResult Delete(long id) {
            var company = context.Companies.FirstOrDefault(t => t.Id == id);
            if (company == null) {
                return NotFound();
            }

            logger.LogDebug("Deleting company: " + company.Name);

            context.Companies.Remove(company);
            context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}/locations/{locationId}", Name = "RemoveRegionLocation")]
        [Authorize(Policy = PermissionNames.RegionsUpdate)]
        public IActionResult RemoveRegionLocation(long id, long locationId) {
            var region = context.Regions.Include(f=>f.RegionLocations).Where(t => t.Id == id).FirstOrDefault();

            var found = region.RegionLocations.Where(f => f.LocationId == locationId).FirstOrDefault();
            if (found == null) {
                return NotFound();
            }
            region.RegionLocations.Remove(found);
            context.SaveChanges();
            return new NoContentResult();
        }

        [HttpPut("{id}/locations/{locationId}", Name = "AddRegionLocation")]
        [Authorize(Policy = PermissionNames.RegionsUpdate)]
        public IActionResult AddRegionLocation(long id, int locationId) {
            var region = context.Regions.Include(f => f.RegionLocations).Where(t => t.Id == id).FirstOrDefault();

            var found = region.RegionLocations.Where(f => f.LocationId == locationId).FirstOrDefault();
            if(found == null) {
                region.RegionLocations.Add(new RegionLocation() { RegionId = region.Id, LocationId = locationId });
                context.SaveChanges();
            }
            
            return new NoContentResult();
        }
    }
}