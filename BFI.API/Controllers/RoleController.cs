﻿using System.Collections.Generic;
using System.Linq;
using BFI.Core.Constants;
using BFI.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BFI.API.Controllers {
    [Produces("application/json")]
    [Route("api/Role")]
    [Authorize(Policy = PolicyNames.BFIAdmin)]
    public class RoleController : Controller {
        private ApplicationDbContext context;
        private ILogger logger;
        public RoleController(ApplicationDbContext context, ILogger<RoleController> _logger) {
            this.context = context;
            this.logger = _logger;
        }

        [HttpGet]
        public IEnumerable<IdentityRole<long>> GetAll() {
            logger.LogDebug("Getting all roles");
            return context.Roles.ToList();
        }

        [HttpGet("{id}", Name = "GetRole")]
        public IActionResult GetById(long id) {
            logger.LogDebug("Getting role: " + id.ToString());
            var item = context.Roles.FirstOrDefault(t => t.Id == id);
            if (item == null) {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpGet("{id}/Users", Name = "GetRoleUsers")]
        public IActionResult Users(long id) {
            logger.LogDebug("Getting users in role: " + id.ToString());
            var role = context.Roles.FirstOrDefault(t => t.Id == id);
            if (role == null) {
                return NotFound();
            }

            var usersInRole = context.UserRoles.Where(f => f.RoleId == role.Id).Select(g => g.UserId).ToArray();
            var users = context.Users.Where(f => usersInRole.Contains(f.Id));

            return new ObjectResult(users);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Role role) {
            if (role == null) {
                return BadRequest();
            }
            logger.LogDebug("Creating role: " + role.Name);

            context.Roles.Add(role);
            context.SaveChanges();

            return CreatedAtRoute("GetRole", new { id = role.Id }, role);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Role role) {
            if (role == null) {
                return BadRequest();
            }
            logger.LogDebug("Updating role: " + role.Name);

            var existing = context.Roles.FirstOrDefault(t => t.Id == id);
            if (existing == null) {
                return NotFound();
            }

            existing.Name = role.Name;

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id) {
            var role = context.Roles.FirstOrDefault(t => t.Id == id);
            if (role == null) {
                return NotFound();
            }

            logger.LogDebug("Deleting role: " + role.Name);

            context.Roles.Remove(role);
            context.SaveChanges();

            return new NoContentResult();
        }
    }
}