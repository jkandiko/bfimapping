﻿using BFI.Core.Constants;
using BFI.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace BFI.API.Controllers {
    [Route("api/[controller]")]
    [Authorize(Policy = PolicyNames.BFIAdmin)]
    public class UserController : Controller {
        private ApplicationDbContext context;
        private ILogger logger;
        public UserController(ApplicationDbContext context, ILogger<UserController> _logger) {
            this.context = context;
            this.logger = _logger;
        }

        [HttpGet]
        public IEnumerable<User> GetAll() {
            logger.LogDebug("Getting all users");
            return context.Users.ToList();
        }

        [HttpGet("{id}", Name = "GetUser")]
        public IActionResult GetById(long id) {
            logger.LogDebug("Getting user: " + id.ToString());
            var item = context.Users.FirstOrDefault(t => t.Id == id);
            if (item == null) {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpGet("{id}/Roles", Name = "GetUserRoles")]
        public IActionResult Roles(long id) {
            logger.LogDebug("Getting roles for user: " + id.ToString());
            var user = context.Users.FirstOrDefault(t => t.Id == id);
            if (user == null) {
                return NotFound();
            }

            var userRoles = context.UserRoles.Where(f => f.UserId == user.Id).Select(g => g.RoleId).ToArray();
            var roles = context.Roles.Where(f => userRoles.Contains(f.Id));

            return new ObjectResult(roles);
        }

        [HttpGet("{id}/Companies", Name = "GetUserCompanies")]
        public IActionResult Companies(long id) {
            logger.LogDebug("Getting companies for user: " + id.ToString());
            var user = context.Users.FirstOrDefault(t => t.Id == id);
            if(user == null) {
                return NotFound();
            }

            var userCompanies = context.Companies.Include(f => f.Users).Where(g => g.Users.Where(h => h.UserId == id).Count() > 0).ToList();
            if (userCompanies != null) {
                foreach(var item in userCompanies) {
                    item.Users = null;
                }
            }
            return new ObjectResult(userCompanies);
        }

        [HttpPost("{id}/AddRole/{roleId}", Name = "AddUserRole")]
        public IActionResult AddRole(long id, long roleId) {
            logger.LogDebug("Getting roles for user: " + id.ToString());
            var user = context.Users.FirstOrDefault(t => t.Id == id);
            if (user == null) {
                return NotFound();
            }

            var userRoles = context.UserRoles.Where(f => f.UserId == user.Id).Select(g => g.RoleId).ToArray();
            if (userRoles.Where(f=>f == roleId).Count() == 0) {
                context.UserRoles.Add(new Microsoft.AspNetCore.Identity.IdentityUserRole<long>() { UserId = id, RoleId = roleId });
                context.SaveChanges();
            }


            return CreatedAtRoute("GetUser", new { id = user.Id }, user);
        }

        [HttpPost("{id}/RemoveRole/{roleId}", Name = "RemoveUserRole")]
        public IActionResult RemoveRole(long id, long roleId) {
            logger.LogDebug("Getting roles for user: " + id.ToString());
            var user = context.Users.FirstOrDefault(t => t.Id == id);
            if (user == null) {
                return NotFound();
            }

            var userRole = context.UserRoles.Where(f => f.UserId == user.Id && f.RoleId == roleId).FirstOrDefault();
            if (userRole != null) {
                context.UserRoles.Remove(userRole);
            }

            context.SaveChanges();

            return new NoContentResult();
        }

        [HttpPost("{id}/AddCompany/{companyId}", Name = "AddUserCompany")]
        public IActionResult AddCompany(long id, long companyId) {
            logger.LogDebug("Getting companies for user: " + id.ToString());
            var user = context.Users.FirstOrDefault(t => t.Id == id);
            if(user == null) {
                return NotFound();
            }

            var userCompany = context.Companies.Where(f => f.Id == companyId).Include(g=>g.Users).FirstOrDefault()?.Users.Where(h=>h.UserId == id).FirstOrDefault();
            if(userCompany == null) {
                context.Companies.Where(f=>f.Id == companyId).First().Users.Add(new CompanyUser { UserId = id, CompanyId = (int)companyId});
                context.SaveChanges();
            }


            return CreatedAtRoute("GetUser", new { id = user.Id }, user);
        }

        [HttpPost("{id}/RemoveCompany/{CompanyId}", Name = "RemoveUserCompany")]
        public IActionResult RemoveCompany(long id, long companyId) {
            logger.LogDebug("Getting companies for user: " + id.ToString());
            var user = context.Users.FirstOrDefault(t => t.Id == id);
            if(user == null) {
                return NotFound();
            }

            var userCompany = context.Companies.Where(f => f.Id == companyId).Include(g => g.Users).FirstOrDefault()?.Users.Where(h => h.UserId == id).FirstOrDefault();
            if(userCompany != null) {
                context.Companies.Where(f=>f.Id == companyId).First().Users.Remove(userCompany);
            }

            context.SaveChanges();

            return new NoContentResult();
        }

        [HttpPost]
        public IActionResult Create([FromBody] User user) {
            if (user == null) {
                return BadRequest();
            }
            logger.LogDebug("Creating user: " + user.UserName);

            context.Users.Add(user);
            context.SaveChanges();

            return CreatedAtRoute("GetUser", new { id = user.Id }, user);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] User user) {
            if (user == null) {
                return BadRequest();
            }
            logger.LogDebug("Updating user: " + user.UserName);

            var existing = context.Users.FirstOrDefault(t => t.Id == id);
            if (existing == null) {
                return NotFound();
            }

            existing.UserName = user.UserName;
            existing.Email = user.Email;

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id) {
            var user = context.Users.FirstOrDefault(t => t.Id == id);
            if (user == null) {
                return NotFound();
            }

            logger.LogDebug("Deleting user: " + user.UserName);

            context.Users.Remove(user);
            context.SaveChanges();

            return new NoContentResult();
        }
    }
}