﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BFI.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BFI.API.Controllers {
    [Route("api/[controller]")]
    public class CountryController : Controller {
        private ApplicationDbContext context;
        private ILogger logger;
        public CountryController(ApplicationDbContext context, ILogger<CountryController> _logger) {
            this.context = context;
            this.logger = _logger;
        }
        [HttpGet]
        public IEnumerable<Country> GetAll() {
            logger.LogDebug("Getting all countries");
            return context.Countries.ToList();
        }
    }
}