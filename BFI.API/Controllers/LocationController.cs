﻿using BFI.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using BFI.Core.Constants;
using System;
using BFI.Core.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using BFI.Core.Models;

namespace BFI.API.Controllers {
    [Route("api/[controller]")]
    public class LocationController : Controller {
        private ApplicationDbContext context;
        private ILogger logger;
        private readonly IHostingEnvironment _environment;
        private IConfiguration _configuration;
        private const string extensionsKey = "validLocationAttachmentFileExtensions";

        public LocationController(ApplicationDbContext context, ILogger<LocationController> _logger, IHostingEnvironment environment, IConfiguration configuration) {
            this.context = context;
            this.logger = _logger;
            this._environment = environment;
            this._configuration = configuration;
        }

        [HttpGet]
        [Authorize(Policy = PolicyNames.BFIAdmin)]
        public IEnumerable<Location> GetAll() {
            logger.LogDebug("Getting all locations");
            return context.Locations.ToList();
        }

        [HttpGet("{id}", Name = "GetLocation")]
        [Authorize(Policy = PermissionNames.LocationsView)]
        public IActionResult GetById(long id) {
            logger.LogDebug("Getting location: " + id.ToString());
            int companyId = CurrentCompanyID();
            
            var item = context.Locations.FirstOrDefault(t => t.Id == id && t.CompanyId == companyId);

            if (item == null) {
                if (User.HasRole("BFI Administrator")) {
                    item = context.Locations.FirstOrDefault(t => t.Id == id);
                    return new ObjectResult(item);
                }
                return NotFound();
            }

            return new ObjectResult(item);
        }


        [HttpGet("{id}/attachments", Name = "LocationAttachments")]
        [Authorize(Policy = PermissionNames.LocationsView)]
        public IActionResult Attachments(long id) {
            logger.LogDebug("Getting location attachments: " + id.ToString());
            var items = context.LocationAttachments.Where(t => t.LocationId == id).ToList();
            return new ObjectResult(items);
        }

        [HttpPost("{id}/attachments", Name = "AddLocationAttachment")]
        [Authorize(Policy = PermissionNames.LocationsUpdate)]
        public async Task<IActionResult> PostAttachment(int id, IFormFile file, string Description) {
            var uploads = Path.Combine(_environment.WebRootPath, "locationattachments");

            if (file.Length > 0) {
                // validate file size

                // validate file extensions
                if (!IsValidExtension(file.FileName)) {
                    return BadRequest();
                }

                // determine file type
                var attachmentType = DetermineAttachmentType(file.FileName);

                var newAttachment = new LocationAttachment();
                try {
                    newAttachment.CreatedBy = User.Name() ?? "API";
                    newAttachment.CreatedOn = DateTime.Now;
                    newAttachment.ModifiedBy = newAttachment.CreatedBy;
                    newAttachment.ModifiedOn = newAttachment.CreatedOn;
                    //newAttachment.Uri = Path.Combine(uploads, file.FileName);
                    newAttachment.Name = file.FileName;
                    newAttachment.Type = attachmentType;
                    newAttachment.Description = Description;
                    newAttachment.LocationId = id;

                    context.LocationAttachments.Add(newAttachment);
                    context.SaveChanges();

                    using (var fileStream = new FileStream(Path.Combine(uploads, newAttachment.Id + "_" + file.FileName), FileMode.Create)) {
                        await file.CopyToAsync(fileStream);
                    }

                    newAttachment.Uri = Path.Combine(uploads, newAttachment.Id + "_" + file.FileName);
                    context.LocationAttachments.Update(newAttachment);
                    context.SaveChanges();

                } catch (Exception ex) {
                    if (newAttachment.Id > 0) {
                        context.LocationAttachments.Remove(newAttachment);
                        context.SaveChanges();
                    }
                }

                return new NoContentResult();
            }

            return BadRequest();
        }

        [HttpGet("{id}/attachments/{attachmentId}", Name = "GetLocationAttachment")]
        [Authorize(Policy = PermissionNames.LocationsView)]
        public IActionResult GetLocationAttachment(long id, long attachmentId) {
            logger.LogDebug("Getting location attachment for location: " + id.ToString() + " with attachment id: " + attachmentId);
            var items = context.LocationAttachments.Where(t => t.Id == attachmentId);
            return new ObjectResult(items);
        }

        [HttpDelete("{id}/attachments/{attachmentId}", Name = "DeleteLocationAttachment")]
        [Authorize(Policy = PermissionNames.LocationsView)]
        public IActionResult DeleteAttachment(long id, long attachmentId) {
            var uploads = Path.Combine(_environment.WebRootPath, "locationattachments");
            var attachment = context.LocationAttachments.Where(t => t.Id == attachmentId).FirstOrDefault();

            FileInfo fi = new FileInfo(attachment.Uri);
            fi.Delete();

            context.LocationAttachments.Remove(attachment);
            context.SaveChanges();
            return new NoContentResult();
        }

        private bool IsValidExtension(string fileName) {
            if (fileName == null) {
                return false;
            }
            string fileExtension = new FileInfo(fileName).Extension.ToLower();

            string[] validExtensions = _configuration[extensionsKey].Split(',');

            return validExtensions.Contains(fileExtension);
        }

        private AttachmentType DetermineAttachmentType(string fileName) {
            string fileExtension = new FileInfo(fileName).Extension;

            if (fileExtension == ".pdf") {
                return AttachmentType.Pdf;
            }

            return AttachmentType.Image;
        }
        [HttpPost]
        [Authorize(Policy = PermissionNames.LocationsCreate)]
        public IActionResult Create([FromBody] Location location) {
            if (location == null) {
                return BadRequest();
            }
            logger.LogDebug("Creating location: " + location.Name);

            var subject = User.Claims.Where(f => f.Type == "sub").FirstOrDefault();
            if (subject == null) {
                logger.LogDebug("No sub claim found");
                return NotFound();
            }
            var asId = long.Parse(subject.Value);
            var companyId = context.Companies.Include(f => f.Users).Where(f => f.Users.Where(g => g.UserId == asId).Count() > 0).FirstOrDefault().Id;

            location.CompanyId = companyId;
            location.CreatedOn = DateTime.Now;
            location.ModifiedOn = DateTime.Now;
            location.CreatedBy = User.Name() ?? "API";
            location.ModifiedBy = location.CreatedBy;

            context.Locations.Add(location);
            context.SaveChanges();

            return CreatedAtRoute("GetLocation", new { id = location.Id }, location);
        }

        [HttpPut("{id}")]
        [Authorize(Policy = PermissionNames.LocationsCreate)]
        public IActionResult Update(long id, [FromBody] Location location) {
            if (location == null) {
                return BadRequest();
            }
            logger.LogDebug("Updating location: " + location.Name);

            int companyId = CurrentCompanyID();

            var existing = context.Locations.FirstOrDefault(t => t.Id == id && t.CompanyId == companyId);
            if (existing == null) {
                if(User.HasRole("BFI Administrator")) {
                    existing = context.Locations.FirstOrDefault(t => t.Id == id);
                } else { 
                    return NotFound();
                }
            }

            existing.Name = location.Name;
            existing.Address = location.Address;
            existing.City = location.City;
            existing.CountryId = location.CountryId;
            existing.Description = location.Description;
            existing.Excerpt = location.Excerpt;
            existing.Latitude = location.Latitude;
            existing.Longitude = location.Longitude;
            existing.PinUri = location.PinUri;
            existing.PostCode = location.PostCode;
            existing.StateProvince = location.StateProvince;
            existing.Type = location.Type;

            context.Update(existing);
            context.SaveChanges();
            
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = PermissionNames.LocationsDelete)]
        public IActionResult Delete(long id) {
            logger.LogDebug("Deleting location: " + id);
            int companyId = CurrentCompanyID();

            var location = context.Locations.FirstOrDefault(t => t.Id == id && t.CompanyId == companyId);
            if (location == null) {
                if(User.HasRole("BFI Administrator")) {
                    location = context.Locations.FirstOrDefault(t => t.Id == id);
                } else {
                    return NotFound();
                }
            }
            

            logger.LogDebug("Deleting location: " + location.Name);

            context.Locations.Remove(location);
            context.SaveChanges();
            return new NoContentResult();
        }

        [EnableCors("PublicPostOnlyPolicy")]
        [Route("search", Name = "Search")]
        [HttpPost]
        public IActionResult Search([FromBody]MapParams options) {
            var results = context
                .Locations
                .Include(g => g.Attachments)
                .Where(f=>
                    f.Latitude >= options.SouthwestLatitude && 
                    f.Latitude <= options.NortheastLatitude && 
                    f.Longitude>= options.SouthwestLongitude &&
                    f.Longitude <= options.NortheastLongitude)
                .ToList();

            foreach (var reg in results) {
                foreach (var att in reg.Attachments) {
                    att.Location = null;
                }
            }

            return new ObjectResult(results);
        }

        [EnableCors("PublicPostOnlyPolicy")]
        [Route("logissue", Name = "LogPublicIssue")]
        [HttpPost]
        public IActionResult LogIssue([FromBody]Issue model) {
            var item = context.Locations.Where(f => f.Id == model.Id).FirstOrDefault();

            if (item == null) {
                return NotFound();
            }
            
            context.IncomingLocationIssues.Add(new IncomingLocationIssue() {
                CreatedBy = User.Name() ?? User.ClientId(),
                CreatedOn = DateTime.Now,
                Description = model.Description,
                LocationId = model.Id,
                Name = "",
                ModifiedBy = User.Name() ?? User.ClientId(),
                ModifiedOn = DateTime.Now

            });
            context.SaveChanges();
            return new ObjectResult(item);
        }

        [Route("my", Name = "GetMyLocations")]
        [Authorize(Policy = PermissionNames.LocationsView)]
        public IActionResult MyLocation() {
            logger.LogDebug("Getting the current locations for user: " + User.Name());

            var subject = User.Claims.Where(f => f.Type == "sub").FirstOrDefault();
            if (subject == null) {
                return Unauthorized();
            }

            long subjectId = 0;
            long.TryParse(subject.Value, out subjectId);

            if(User.HasRole(RoleNames.BFIAdministrator)) {
                var locations = context.Locations.Take(200);
                return new ObjectResult(locations);
            } else {
                var companies = context.Companies.Where(f => f.Users.Where(g => g.UserId == subjectId).Count() > 0).Select(g => g.Id);

                var locations = context.Locations.Where(f => companies.Contains(f.CompanyId));
                return new ObjectResult(locations);
            }
        }

        private int CurrentCompanyID() {
            var subject = User.Claims.Where(f => f.Type == "sub").FirstOrDefault();
            if (subject == null) {
                return 0;
            }
            var asId = long.Parse(subject.Value);
            var companyId = context.Companies.Include(f => f.Users).Where(f => f.Users.Where(g => g.UserId == asId).Count() > 0).FirstOrDefault().Id;

            return companyId;
        }
    }
}