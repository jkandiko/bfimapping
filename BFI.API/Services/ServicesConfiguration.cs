﻿using BFI.API.Models.IssueSteps;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.API.Services {
    public static class ServicesConfiguration {
        public static void AddIssueDispatcher(this IServiceCollection services) {
            services.AddSingleton<IDispatchStepOptionsService, DispatchStepOptionsService>();
            services.AddSingleton<IHostedService, IssueDispatcher>();
        }
    }
}
