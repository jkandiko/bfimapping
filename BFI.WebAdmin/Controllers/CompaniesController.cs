﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BFI.Core.Constants;
using BFI.Core.Extensions;
using BFI.Entities;
using BFI.WebAdmin.Models;
using BFI.WebAdmin.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BFI.WebAdmin.Controllers {
    
    public class CompaniesController : BaseController {
        private CompanyRepository repo;
        
        public CompaniesController(CompanyRepository repo, CountryRepository countryRepo, IConfiguration configuration) : base(countryRepo, configuration) {
            this.repo = repo;
        }

        [Authorize(Policy = PermissionNames.CompaniesView)]
        public async Task<IActionResult> Index() {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            var model = new CompaniesViewModel();
            if (User.HasRole(RoleNames.BFIAdministrator)) {
                model.Companies = await repo.GetAll(accessToken);
            } else {
                model.Companies = await repo.GetMy(accessToken);
            }

            return View(model);
        }
        [Authorize(Policy = PermissionNames.CompaniesView)]
        public async Task<IActionResult> View(int id) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var company = await repo.Get(accessToken, id);
            var companyUsers = await repo.UsersInCompany(accessToken, id);
            
            var model = new CompanyViewModel();
            model.Company = company;
            model.Users = companyUsers;
            
            return View(model);
        }
        
        [HttpPost]
        [Authorize(Policy = PermissionNames.CompaniesDelete)]
        public async Task<IActionResult> Delete(int id) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var result = await repo.Delete(accessToken, id);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Authorize(Policy = PermissionNames.CompaniesCreate)]
        public IActionResult Create() {
            var viewModel = new CreateCompanyViewModel();
            viewModel.Company = new Entities.Company();

            return View(viewModel);
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.CompaniesCreate)]
        public async Task<IActionResult> Create([FromForm] Company company) {
            if (!ModelState.IsValid) {
                var viewModel = new CreateCompanyViewModel();
                viewModel.Company = company;
                return View(viewModel);
            }

            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var ret = await repo.Create(accessToken, company);

            return RedirectToAction(nameof(View), new { id = ret.Id });
        }

        [HttpGet]
        [Authorize(Policy = PermissionNames.CompaniesUpdate)]
        public async Task<IActionResult> Edit(int id) {
            var viewModel = new EditCompanyViewModel();
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            viewModel.Company = await repo.Get(accessToken, id);

            return View(viewModel);
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.CompaniesUpdate)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromForm] Company company) {
            if (!ModelState.IsValid) {
                var viewModel = new EditCompanyViewModel();
                viewModel.Company = company;
                return View(viewModel);
            }
            company.Id = id;

            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var ret = await repo.Update(accessToken, company);

            return RedirectToAction(nameof(View), new { id = ret.Id });
        }
    }
}