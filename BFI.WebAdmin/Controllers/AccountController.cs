﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Controllers {
    public class AccountController : Controller{
        private readonly ILogger _logger;

        public AccountController(ILogger<AccountController> logger) {
            _logger = logger;
        }

        [Authorize]
        public async Task<IActionResult> Login() {
            _logger.LogDebug("Is user authenticated: " + User.Identity.IsAuthenticated.ToString());
            _logger.LogDebug("User: " + User.Identity?.Name?.ToString());
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task Logout() {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
        }

        [HttpGet]
        public async Task<IActionResult> FrontChannelUri() {
            _logger.LogDebug("Front channel called");
            await HttpContext.SignOutAsync();
            return Redirect("/");
        }
    }
}
