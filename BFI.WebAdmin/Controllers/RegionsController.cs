﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BFI.Core.Constants;
using BFI.Entities;
using BFI.WebAdmin.Models;
using BFI.WebAdmin.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BFI.WebAdmin.Controllers {
    
    public class RegionsController : BaseController {
        private RegionRepository repo;
        private LocationRepository locationRepo;

        public RegionsController(RegionRepository repo, LocationRepository locationRepo, CountryRepository countryRepo, IConfiguration configuration) : base(countryRepo, configuration) {
            this.repo = repo;
            this.locationRepo = locationRepo;
        }
        [Authorize(Policy = PermissionNames.RegionsView)]
        public async Task<IActionResult> Index() {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var regions = await repo.GetAll(accessToken);

            var model = new RegionsViewModel();
            model.Regions = regions;

            return View(model);
        }
        [Authorize(Policy = PermissionNames.RegionsView)]
        public async Task<IActionResult> View(int id) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var region = await repo.Get(accessToken, id);

            var model = new RegionViewModel();
            model.Region = region;
            model.LocationViewModels = new List<LocationViewModel>();

            foreach (var item in region.RegionLocations) {
                model.LocationViewModels.Add(new LocationViewModel() { Location = item.Location, AttachmentUri = LocationAttachmentUri });
            }

            return View(model);
        }

        [Authorize(Policy = PermissionNames.RegionsCreate)]
        public IActionResult Create() {
            var model = new CreateRegionViewModel();
            model.Region = new Entities.Region() { InitialZoom = 12 };
            return View(model);
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.RegionsCreate)]
        public async Task<IActionResult> Create([FromForm] Region region) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            if(!ModelState.IsValid) {
                var viewModel = new CreateRegionViewModel();
                viewModel.Region = region;
                return View(viewModel);
            }

            var ret = await repo.Create(accessToken, region);

            return RedirectToAction(nameof(View), new { id = ret.Id });
        }

        [HttpGet]
        [Authorize(Policy = PermissionNames.RegionsUpdate)]
        public async Task<IActionResult> Edit(int id) {
            var viewModel = new EditRegionViewModel();
            //var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var accessToken = await base.GetAccessToken();

            var myLocations = await locationRepo.GetMy(accessToken);
            viewModel.MyLocations = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>();
            foreach(var item in myLocations) {
                viewModel.MyLocations.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem() { Text = item.Name, Value = item.Id.ToString() });
            }

            viewModel.MyLocations = viewModel.MyLocations.OrderBy(f => f.Text).ToList();

            viewModel.Region = await repo.Get(accessToken, id);

            foreach(var item in viewModel.Region.RegionLocations) {
                viewModel.LocationViewModels.Add(new LocationViewModel() { Location = item.Location, AttachmentUri = LocationAttachmentUri });
            }
            return View(viewModel);
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.RegionsUpdate)]
        public async Task<IActionResult> RemoveLocation(int id, int locationId) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var result = await repo.RemoveLocation(accessToken, id, locationId);

            return RedirectToAction(nameof(Edit), new { id = id });
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.RegionsUpdate)]
        public async Task<IActionResult> AddLocation(int id, int SelectedLocationId) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var result = await repo.AddLocation(accessToken, id, SelectedLocationId);

            return RedirectToAction(nameof(Edit), new { id = id });
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.RegionsUpdate)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromForm] Region region) {
            if(!ModelState.IsValid) {
                var viewModel = new EditRegionViewModel();
                viewModel.Region = region;
                return View("Edit", viewModel);
            }
            region.Id = id;

            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var ret = await repo.Update(accessToken, region);

            return RedirectToAction(nameof(Edit), new { id = ret.Id });
        }
    }
}