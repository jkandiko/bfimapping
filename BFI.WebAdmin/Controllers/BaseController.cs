﻿using BFI.Core.Constants;
using BFI.WebAdmin.Repositories;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Controllers { 
    public abstract class BaseController : Controller {
        private CountryRepository countryRepo;
        public IConfiguration Configuration { get; set; }
        public BaseController(CountryRepository countryRepo, IConfiguration configuration) {
            this.countryRepo = countryRepo;
            this.Configuration = configuration;
        }

        public override void OnActionExecuting(ActionExecutingContext context) {
            ViewData["googleMapsApiKey"] = Configuration["GoogleMapsApiKey"];
            base.OnActionExecuting(context);
        }

        public string LocationAttachmentUri {
            get {
                return Configuration["attachmentLocationUri"];
            }
        }

        public async Task<string> GetAccessToken() {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            var jwthandler = new JwtSecurityTokenHandler();
            var jwttoken = jwthandler.ReadToken(accessToken);
            var expDate = jwttoken.ValidTo;
            if(expDate < DateTime.UtcNow.AddMinutes(1)) {
                var disco = await DiscoveryClient.GetAsync(Configuration["IdentityServerAddress"]);
                if(disco.IsError)
                    throw new Exception(disco.Error);

                var tokenClient = new TokenClient(disco.TokenEndpoint, Configuration["clientId"], Configuration["clientSecret"]);
                var rt = await HttpContext.GetTokenAsync("refresh_token");
                var tokenResult = await tokenClient.RequestRefreshTokenAsync(rt);

                if(!tokenResult.IsError) {
                    var old_id_token = await HttpContext.GetTokenAsync("id_token");
                    var new_access_token = tokenResult.AccessToken;
                    var new_refresh_token = tokenResult.RefreshToken;

                    var tokens = new List<AuthenticationToken>();
                    tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.IdToken, Value = old_id_token });
                    tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.AccessToken, Value = new_access_token });
                    tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.RefreshToken, Value = new_refresh_token });

                    var expiresAt = DateTime.UtcNow + TimeSpan.FromSeconds(tokenResult.ExpiresIn);
                    tokens.Add(new AuthenticationToken { Name = "expires_at", Value = expiresAt.ToString("o", CultureInfo.InvariantCulture) });

                    var info = await HttpContext.AuthenticateAsync("Cookies");
                    info.Properties.StoreTokens(tokens);
                    await HttpContext.SignInAsync("Cookies", info.Principal, info.Properties);

                }
                accessToken = GetAccessToken().Result;
            }

            return accessToken;
        }

        public async Task<List<SelectListItem>> Countries(string accessToken) {
            var countries = await countryRepo.GetAll(accessToken);

            var allCountries = new System.Collections.Generic.List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>();

            foreach (var country in countries) {
                allCountries.Add(new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem() { Text = country.Name, Value = country.Id.ToString() });
            }

            allCountries = allCountries.OrderBy(f => f.Text).ToList();

            return allCountries;
        }
    }
}
