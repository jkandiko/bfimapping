﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BFI.Core.Constants;
using BFI.WebAdmin.Models;
using BFI.WebAdmin.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BFI.WebAdmin.Controllers {
    [Authorize(Policy = PolicyNames.BFIAdmin)]
    public class RolesController : Controller {
        private RoleRepository repo;
        public RolesController(RoleRepository repo) {
            this.repo = repo;
        }

        public async Task<IActionResult> Index() {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var roles = await repo.GetAll(accessToken);

            var model = new RolesViewModel();
            model.Roles = roles;

            return View(model);
        }

        public async Task<IActionResult> View(int id) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var role = await repo.Get(accessToken, id);
            var usersInRole = await repo.UsersInRole(accessToken, id);
            var model = new RoleViewModel();
            model.Role = role;
            model.UsersInRole = usersInRole;
            return View(model);
        }
    }
}
