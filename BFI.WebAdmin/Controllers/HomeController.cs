﻿using BFI.Core.Constants;
using BFI.WebAdmin.Models;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Controllers {
    public class HomeController : Controller {
        private readonly ILogger _logger;

        public HomeController(ILogger<HomeController> logger) {
            _logger = logger;
        }
        public IActionResult Index() {
            _logger.LogDebug("Is user authenticated: " + User.Identity.IsAuthenticated.ToString());
            _logger.LogDebug("User: " + User.Identity?.Name?.ToString());
            return View();
        }

        [Authorize]
        public IActionResult Secure() {
            ViewData["Message"] = "Secure page.";

            return View();
        }

        [Authorize(Policy = "BFIAdmin")]
        public IActionResult AdminPage() {
            ViewData["Message"] = "Admin only page";
            return View();
        }


        public IActionResult Error() {
            _logger.LogError("Error: " + Activity.Current?.Id ?? HttpContext.TraceIdentifier);
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        public async Task<IActionResult> CallApiUsingClientCredentials() {
            var tokenClient = new TokenClient("http://localhost:5000/connect/token", "internal.MVCClient", "bfi-webapp-secret");
            var tokenResponse = await tokenClient.RequestClientCredentialsAsync("BFI_API_V1");

            var client = new HttpClient();
            client.SetBearerToken(tokenResponse.AccessToken);
            var content = await client.GetStringAsync("http://localhost:5001/identity");

            ViewBag.Json = JArray.Parse(content).ToString();
            return View("json");
        }

        public async Task<IActionResult> CallApiUsingUserAccessToken() {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            var content = await client.GetStringAsync("http://localhost:5001/identity");

            ViewBag.Json = JArray.Parse(content).ToString();
            return View("json");
        }
    }
}
