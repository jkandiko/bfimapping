﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BFI.Core.Constants;
using BFI.Entities;
using BFI.WebAdmin.Models;
using BFI.WebAdmin.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BFI.WebAdmin.Controllers {
    [Authorize(Policy = PolicyNames.BFIAdmin)]
    public class UsersController : BaseController {
        private UserRepository repo;
        private RoleRepository rolesRepo;
        private CompanyRepository companyRepo;
        public UsersController(UserRepository repo, RoleRepository rolesRepo, CountryRepository countryRepo, CompanyRepository companyRepo, IConfiguration configuration) : base(countryRepo, configuration) {
            this.repo = repo;
            this.rolesRepo = rolesRepo;
            this.companyRepo = companyRepo;
        }

        public async  Task<IActionResult> Index() {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var users = await repo.GetAll(accessToken);

            var model = new UsersViewModel();
            model.Users = users;

            return View(model);
        }

        public async Task<IActionResult> View(int id) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var user = await repo.Get(accessToken, id);
            var usersRoles = await repo.UserRoles(accessToken, id);
            var userCompanies = await repo.UserCompanies(accessToken, id);
            var roles = await rolesRepo.GetAll(accessToken);
            var companies = await companyRepo.GetAll(accessToken);
            var model = new UserViewModel();
            model.User = user;
            model.UsersRoles = usersRoles;
            model.UsersCompanies = userCompanies;
            model.AllRoles = roles;
            model.AllCompanies = companies;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AddToRole(int id, [FromForm] int SelectedRoleId) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var ret = await repo.AddRole(accessToken, id, SelectedRoleId);
           
            return RedirectToAction("View", new { id = id });
        }
        [HttpPost]
        public async Task<IActionResult> RemoveRole(int id, [FromForm] int roleId) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var ret = await repo.RemoveRole(accessToken, id, roleId);
           
            return RedirectToAction("View", new { id = id });
        }

        [HttpPost]
        public async Task<IActionResult> AddToCompany(int id, [FromForm] int SelectedCompanyId) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var ret = await repo.AddCompany(accessToken, id, SelectedCompanyId);

            return RedirectToAction("View", new { id = id });
        }
        [HttpPost]
        public async Task<IActionResult> RemoveCompany(int id, [FromForm] int companyId) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var ret = await repo.RemoveCompany(accessToken, id, companyId);

            return RedirectToAction("View", new { id = id });
        }

    }
}