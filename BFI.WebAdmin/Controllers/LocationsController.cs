﻿using BFI.Core.Constants;
using BFI.Core.Extensions;
using BFI.Entities;
using BFI.WebAdmin.Models;
using BFI.WebAdmin.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Controllers {
    
    public class LocationsController : BaseController {
        private LocationRepository repo;
        private UserRepository usersRepo;


        public LocationsController(LocationRepository repo, UserRepository usersRepo, CountryRepository countryRepo, IConfiguration configuration) : base(countryRepo, configuration) {
            this.repo = repo;
            this.usersRepo = usersRepo;
        }
        [Authorize(Policy = PermissionNames.LocationsView)]
        public async Task<IActionResult> Index() {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            var model = new LocationsViewModel();
            if (User.HasRole(RoleNames.BFIAdministrator)) {
                model.Locations = await repo.GetAll(accessToken);
            } else {
                model.Locations = await repo.GetMy(accessToken);
            }

            return View(model);
        }
        [Authorize(Policy = PermissionNames.LocationsView)]
        public async Task<IActionResult> View(int id) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var location = await repo.Get(accessToken, id);

            if(location == null) {
                return NotFound();
            }
            var model = new LocationViewModel();
            model.Location = location;
            model.AttachmentUri = LocationAttachmentUri;
            return View(model);
        }

        [HttpGet]
        [Authorize(Policy = PermissionNames.LocationsCreate)]
        public async Task<IActionResult> Create() {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            var viewModel = new CreateLocationViewModel();
            viewModel.Location = new Entities.Location();

            viewModel.AllCountries = await base.Countries(accessToken);
            viewModel.Location.CountryId = int.Parse(viewModel.AllCountries.Where(f => f.Text == "United States").FirstOrDefault().Value);
            return View(viewModel);
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.LocationsCreate)]
        public async Task<IActionResult> Create([FromForm] Location location) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            if (!ModelState.IsValid) {
                var viewModel = new CreateLocationViewModel();
                viewModel.Location = location;

                viewModel.AllCountries = await base.Countries(accessToken);

                return View(viewModel);
            }

            var ret = await repo.Create(accessToken, location);

            return RedirectToAction(nameof(View), new { id = ret.Id });
        }

        [HttpGet]
        [Authorize(Policy = PermissionNames.LocationsUpdate)]
        public async Task<IActionResult> Edit(int id) {
            var viewModel = new EditLocationViewModel();
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            viewModel.Location = await repo.Get(accessToken, id);
            if (viewModel.Location == null) {
                return NotFound();
            }
            viewModel.AllCountries = await base.Countries(accessToken);
            return View(viewModel);
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.LocationsUpdate)]
        public async Task<IActionResult> Update([FromRoute] int id, [FromForm] Location location) {
            if (!ModelState.IsValid) {
                var viewModel = new EditLocationViewModel();
                viewModel.Location = location;
                return View("Edit", viewModel);
            }
            location.Id = id;

            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var ret = await repo.Update(accessToken, location);

            return RedirectToAction(nameof(Edit), new { id = ret.Id });
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.LocationsDelete)]
        public async Task<IActionResult> Delete(int id) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var result = await repo.Delete(accessToken, id);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.LocationsUpdate)]
        public async Task<IActionResult> CreateAttachment([FromRoute] int id, string Description, IFormFile file) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);

            await repo.CreateAttachment(accessToken, id, file, Description);

            return RedirectToAction(nameof(Edit), new { id = id });
        }

        [HttpPost]
        [Authorize(Policy = PermissionNames.LocationsDelete)]
        public async Task<IActionResult> DeleteAttachment(int id, int attachmentId) {
            var accessToken = await HttpContext.GetTokenAsync(TokenNames.AccessToken);
            var result = await repo.DeleteAttachment(accessToken, id, attachmentId);

            return RedirectToAction(nameof(Edit), new { id = id });
        }
    }
}
