﻿using BFI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Models {
    public class CompanyViewModel {
        public Company Company { get; set; }
        public List<User> Users { get; set; }
    }
    public class CompaniesViewModel {
        public List<Company> Companies { get; set; }
    }
    public class CreateCompanyViewModel {
        public Company Company { get; set; }
    }

    public class EditCompanyViewModel {
        public Company Company { get; set; }
    }
}
