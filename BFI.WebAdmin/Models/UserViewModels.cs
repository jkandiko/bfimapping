﻿using BFI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Models
{
    public class UsersViewModel {
        public List<User> Users { get; set; }
    }

    public class UserViewModel {
        public User User { get; set; }
        public List<Role> UsersRoles { get; set; }
        public List<Company> UsersCompanies { get; set; }
        public int SelectedRoleId { get; set; }
        public List<Role> AllRoles { get; set; }
        public List<Company> AllCompanies { get; set; }
        public int SelectedCompanyId { get; set; }
    }
}
