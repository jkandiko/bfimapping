﻿using BFI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Models
{
    public class RoleViewModel {
        public Role Role { get; set; }
        public List<User> UsersInRole { get; set; }
    }
    public class RolesViewModel {
        public List<Role> Roles { get; set; }
    }
}
