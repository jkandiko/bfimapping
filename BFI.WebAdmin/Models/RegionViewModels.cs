﻿using BFI.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Models {
    public class RegionViewModel{
        public Region Region { get; set; }
        public List<LocationViewModel> LocationViewModels { get; set; }
    }

    public class RegionsViewModel {
        public List<Region> Regions { get; set; }
    }

    public class CreateRegionViewModel {
        public Region Region { get; set; }
    }

    public class EditRegionViewModel {
        public Region Region { get; set; }

        public List<LocationViewModel> LocationViewModels { get; set; } 

        public List<SelectListItem> MyLocations { get; set; }
        public int SelectedLocationId { get; set; }

        public EditRegionViewModel() {
            LocationViewModels = new List<LocationViewModel>();
        }
    }
}
