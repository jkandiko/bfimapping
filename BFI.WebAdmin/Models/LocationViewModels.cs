﻿using BFI.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Models {
    public abstract class BaseLocationViewModel {
        public string AttachmentUri { get; set; }
    }
    public class LocationViewModel : BaseLocationViewModel {

        public Location Location { get; set; }
            
    }
    public class LocationsViewModel : BaseLocationViewModel {
        public List<Location> Locations { get; set; }
    }

    public class CreateLocationViewModel : BaseLocationViewModel {
        public Location Location { get; set; }
        public List<SelectListItem> AllCountries { get; set; }

    }

    public class EditLocationViewModel : BaseLocationViewModel {
        public Location Location { get; set; }
        public List<SelectListItem> AllCountries { get; set; }
    }
}
