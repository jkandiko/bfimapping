﻿$(function () {
    $("#delete-confirm").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Yes": function () {
                $("#delete-confirm").data('formToPost').submit();
            },
            No: function () {
                $(this).dialog("close");
            }
        }
    });

    $('.delete').on('click', function (e) {
        $("#delete-confirm")
            .data('formToPost', $(this).parent("form"))
            .dialog('open');
        return false;
    });
});