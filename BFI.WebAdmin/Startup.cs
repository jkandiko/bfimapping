﻿using BFI.Core.Constants;
using BFI.Core.Security;
using BFI.WebAdmin.Repositories;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.WebAdmin {
    public class Startup {
        public IHostingEnvironment HostingEnvironment { get; }
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) {
            var builder = new ConfigurationBuilder();

            builder.AddUserSecrets<Startup>();  

            Configuration = builder.Build();

            foreach (var item in configuration.AsEnumerable()) {
                Configuration[item.Key] = item.Value;
            }
        }

        public Startup(IHostingEnvironment env) {
            var builder = new ConfigurationBuilder();

            if (env.IsDevelopment()) {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }

        public Startup(IHostingEnvironment env, IConfiguration config) {
            HostingEnvironment = env;
            Configuration = config;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services
                .AddAuthentication(options => {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
                })
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options => {
                    options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    
                    options.Authority = Configuration["IdentityServerAddress"];
                    options.RequireHttpsMetadata = false;

                    options.ClientId = Configuration["clientId"];
                    options.ClientSecret = Configuration["clientSecret"];
                    options.ResponseType = "code id_token";

                    options.SaveTokens = true;
                    options.GetClaimsFromUserInfoEndpoint = true;

                    options.SignedOutCallbackPath = Configuration["signedOutCallbackPath"];
                    options.SignedOutRedirectUri = Configuration["signedOutRedirectUri"];
                    
                    options.Scope.Add(Configuration["apiName"]);
                    options.Scope.Add("offline_access");
                    
                    options.Events.OnTokenValidated = ctx => {
                        var s = ctx.Principal.Identity.Name;
                        return Task.CompletedTask;
                    };

                    
                    
                    options.Events.OnUserInformationReceived = ctx => {
                        //var nameclaim = ctx.User.ToClaims().Where(f=>f.Subject.Name == "name").FirstOrDefault();

                        
                        return Task.CompletedTask;
                    };
                    options.Events.OnTokenResponseReceived = ctx => {
                        return Task.CompletedTask;
                    };
                    options.Events.OnAuthenticationFailed = ctx => {
                        return Task.CompletedTask;
                    };
                });

            services.AddAuthorization(options => {
                foreach (var item in Policies.Build()) {
                    options.AddPolicy(item.Key, item.Value);
                }
            });

            services.AddSingleton<IAuthorizationHandler, RoleRequirementHandler>();
            services.AddSingleton<IAuthorizationHandler, PermissionRequirementHandler>();
            services.AddTransient<UserRepository, UserRepository>();
            services.AddTransient<RoleRepository, RoleRepository>();
            services.AddTransient<CompanyRepository, CompanyRepository>();
            services.AddTransient<RegionRepository, RegionRepository>();
            services.AddTransient<LocationRepository, LocationRepository>();
            services.AddTransient<CountryRepository, CountryRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            } else {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseMvcWithDefaultRoute();
        }
    }
}
