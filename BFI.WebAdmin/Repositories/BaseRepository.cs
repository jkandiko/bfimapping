﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Repositories { 
    public abstract class BaseRepository {

        public HttpClient SetUpApiClient(string accessToken, string apiUrl) {
            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(apiUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}
