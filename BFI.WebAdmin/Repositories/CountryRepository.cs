﻿using BFI.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Repositories {
    public class CountryRepository : BaseRepository {
        private readonly ILogger _logger;
        private IConfiguration configuration;

        public CountryRepository(ILogger<CountryRepository> logger, IConfiguration configuration) {
            _logger = logger;
            this.configuration = configuration;
        }
        public async Task<List<Country>> GetAll(string accessToken) {
            var ret = new List<Country>();

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.GetAsync("/api/Country");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Country>>();
            }

            return ret;
        }
    }
}
