﻿using BFI.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Repositories {
    public class LocationRepository : BaseRepository {
        private readonly ILogger _logger;
        private IConfiguration configuration;

        public LocationRepository(ILogger<LocationRepository> logger, IConfiguration configuration) {
            _logger = logger;
            this.configuration = configuration;
        }
        public async Task<List<Location>> GetAll(string accessToken) {
            var ret = new List<Location>();

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.GetAsync("/api/Location");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Location>>();
            }

            return ret;
        }

        public async Task<List<Location>> GetMy(string accessToken) {
            var ret = new List<Location>();

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]); 

            HttpResponseMessage response = await client.GetAsync("/api/Location/my");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Location>>();
            }

            return ret;
        }

        public async Task<Location> Get(string accessToken, int id) {
            var ret = new Location() { Attachments = new List<LocationAttachment>() };

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.GetAsync($"/api/Location/{id}");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Location>();
            } else {
                return null;
            }

            response = await client.GetAsync($"/api/Location/{id}/attachments");
            if (response.IsSuccessStatusCode) {
                ret.Attachments = await response.Content.ReadAsAsync<List<LocationAttachment>>();
            }

            return ret;
        }

        public async Task<LocationAttachment> CreateAttachment(string accessToken, int id, IFormFile file, string description) {
            LocationAttachment ret = null;

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            var form = new MultipartFormDataContent();
            var fileContent = new StreamContent(file.OpenReadStream());
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data") { Name = "\"file\"", FileName = "\"" + file.FileName + "\"" };
            fileContent.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);
            form.Add(fileContent);
            form.Add(new StringContent(description), "Description");
            HttpResponseMessage response = await client.PostAsync($"/api/Location/{id}/attachments", form);
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<LocationAttachment>();
            }

            return ret;
        }

        public async Task<Location> Create(string accessToken, Location location) {
            Location ret = null;
            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);
            var json = JsonConvert.SerializeObject(location);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync($"/api/Location", stringContent);
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Location>();
            }

            return ret;
        }

        public async Task<Location> Update(string accessToken, Location location) {
            Location ret = null;
            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);
            var json = JsonConvert.SerializeObject(location);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync($"/api/Location/{location.Id}", stringContent);
            if (response.IsSuccessStatusCode) {
                ret = location;
            }

            return ret;
        }

        public async Task<bool> Delete(string accessToken, long id) {
            var ret = false;

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.DeleteAsync($"/api/Location/{id}");

            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<bool>();
            }

            return ret;
        }

        public async Task<bool> DeleteAttachment(string accessToken, long id, long attachmentId) {
            var ret = false;

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.DeleteAsync($"/api/Location/{id}/attachments/{attachmentId}");

            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<bool>();
            }

            return ret;
        }
    }
}
