﻿using BFI.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Repositories {
    public class CompanyRepository : BaseRepository {
        private readonly ILogger _logger;
        private IConfiguration configuration;

        public CompanyRepository(ILogger<CompanyRepository> logger, IConfiguration configuration) {
            _logger = logger;
            this.configuration = configuration;
        }
        public async Task<List<Company>> GetAll(string accessToken) {
            var ret = new List<Company>();

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.GetAsync("/api/Company");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Company>>();
            }

            return ret;
        }

        public async Task<List<Company>> GetMy(string accessToken) {
            var ret = new List<Company>();

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.GetAsync("/api/Company/my");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Company>>();
            }

            return ret;
        }

        public async Task<Company> Get(string accessToken, int id) {
            var ret = new Company();

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.GetAsync($"/api/Company/{id}");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Company>();
            }

            return ret;
        }

        public async Task<List<User>> UsersInCompany(string accessToken, int id) {
            var ret = new List<User>();

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.GetAsync($"/api/Company/{id}/Users");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<User>>();
            }

            return ret;
        }

        public async Task<Company> RemoveUser(string accessToken, long id, int userId) {
            var ret = new Company();

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.PostAsync($"/api/Company/{id}/RemoveUser/{userId}", new StringContent(""));
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Company>();
            }

            return ret;
        }

        public async Task<Company> Create(string accessToken, Company company) {
            Company ret = null;
            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);
            var json = JsonConvert.SerializeObject(company);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync($"/api/Company", stringContent);
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Company>();
            }

            return ret;
        }

        public async Task<Company> Update(string accessToken, Company company) {
            Company ret = null;
            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);
            var json = JsonConvert.SerializeObject(company);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync($"/api/Company/{company.Id}", stringContent);
            if (response.IsSuccessStatusCode) {
                ret = company;
            }

            return ret;
        }

        public async Task<bool> Delete(string accessToken, long id) {
            var ret = false;

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.DeleteAsync($"/api/Company/{id}");

            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<bool>();
            }

            return ret;
        }
    }
}
