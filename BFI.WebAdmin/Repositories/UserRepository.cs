﻿using BFI.Entities;
using BFI.WebAdmin.Models;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Repositories {
    public class UserRepository {
        private readonly ILogger _logger;
        private IConfiguration configuration;

        public UserRepository(ILogger<UserRepository> logger, IConfiguration configuration) {
            _logger = logger;
            this.configuration = configuration;
        }
        public async Task<List<User>> GetAll(string accessToken) {
            var ret = new List<User>();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync("/api/User");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<User>>();
            }

            return ret;
        }

        public async Task<User> Get(string accessToken, long id) {
            var ret = new User();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync($"/api/User/{id}");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<User>();
            }

            return ret;
        }

        public async Task<List<Role>> UserRoles(string accessToken, long id) {
            var ret = new List<Role>();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync($"/api/User/{id}/Roles");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Role>>();
            }

            return ret;
        }
        public async Task<List<Company>> UserCompanies(string accessToken, long id) {
            var ret = new List<Company>();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync($"/api/User/{id}/Companies");
            if(response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Company>>();
            }

            return ret;
        }
        public async Task<User> AddRole(string accessToken, long id, int roleId) {
            var ret = new User();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.PostAsync($"/api/User/{id}/AddRole/{roleId}", new StringContent(""));
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<User>();
            }

            return ret;
        }

        public async Task<User> RemoveRole(string accessToken, long id, int roleId) {
            var ret = new User();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.PostAsync($"/api/User/{id}/RemoveRole/{roleId}", new StringContent(""));
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<User>();
            }

            return ret;
        }

        public async Task<User> AddCompany(string accessToken, long id, int companyId) {
            var ret = new User();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.PostAsync($"/api/User/{id}/AddCompany/{companyId}", new StringContent(""));
            if(response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<User>();
            }

            return ret;
        }

        public async Task<User> RemoveCompany(string accessToken, long id, int companyId) {
            var ret = new User();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.PostAsync($"/api/User/{id}/RemoveCompany/{companyId}", new StringContent(""));
            if(response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<User>();
            }

            return ret;
        }
    }
}
