﻿using BFI.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Repositories {
    public class RoleRepository {
        private readonly ILogger _logger;
        private IConfiguration configuration;

        public RoleRepository(ILogger<RoleRepository> logger, IConfiguration configuration) {
            _logger = logger;
            this.configuration = configuration;
        }
        public async Task<List<Role>> GetAll(string accessToken) {
            var ret = new List<Role>();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync("/api/Role");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Role>>();
            }

            return ret;
        }

        public async Task<Role> Get(string accessToken, int id) {
            var ret = new Role();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync($"/api/Role/{id}");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Role>();
            }

            return ret;
        }

        public async Task<List<User>> UsersInRole(string accessToken, int id) {
            var ret = new List<User>();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync($"/api/Role/{id}/Users");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<User>>();
            }

            return ret;
        }
    }
}
