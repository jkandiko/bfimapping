﻿using BFI.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BFI.WebAdmin.Repositories {
    public class RegionRepository : BaseRepository {
        private readonly ILogger _logger;
        private IConfiguration configuration;

        public RegionRepository(ILogger<RegionRepository> logger, IConfiguration configuration) {
            _logger = logger;
            this.configuration = configuration;
        }
        public async Task<List<Region>> GetAll(string accessToken) {
            var ret = new List<Region>();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync("/api/Region");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<List<Region>>();
            }

            return ret;
        }

        public async Task<Region> Get(string accessToken, int id) {
            var ret = new Region();

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            client.BaseAddress = new Uri(configuration["apiUri"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await client.GetAsync($"/api/Region/{id}");
            if (response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Region>();
            }

            return ret;
        }

        public async Task<Region> Create(string accessToken, Region region) {
            Region ret = null;
            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);
            var json = JsonConvert.SerializeObject(region);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync($"/api/Region", stringContent);
            if(response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<Region>();
            }

            return ret;
        }

        public async Task<Region> Update(string accessToken, Region region) {
            Region ret = null;
            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);
            var json = JsonConvert.SerializeObject(region);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PutAsync($"/api/Region/{region.Id}", stringContent);
            if(response.IsSuccessStatusCode) {
                ret = region;
            }

            return ret;
        }

        public async Task<bool> Delete(string accessToken, long id) {
            var ret = false;

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.DeleteAsync($"/api/Region/{id}");

            if(response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<bool>();
            }

            return ret;
        }

        public async Task<bool> RemoveLocation(string accessToken, long id, long locationId) {
            var ret = false;

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.DeleteAsync($"/api/Region/{id}/locations/{locationId}");

            if(response.IsSuccessStatusCode) {
                ret = await response.Content.ReadAsAsync<bool>();
            }

            return ret;
        }

        public async Task<bool> AddLocation(string accessToken, long id, long locationId) {
            var ret = false;

            var client = base.SetUpApiClient(accessToken, configuration["apiUri"]);

            HttpResponseMessage response = await client.PutAsync($"/api/Region/{id}/locations/{locationId}", new StringContent(""));

            if(response.IsSuccessStatusCode) {
                return true;
            }

            return false;
        }
    }
}
