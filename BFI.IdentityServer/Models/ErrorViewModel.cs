using IdentityServer4.Models;
using System;

namespace BFI.IdentityServer.Models
{
    public class ErrorViewModel {
        public ErrorMessage Error { get; set; }
    }
}