﻿using BFI.Entities;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BFI.IdentityServer
{
    public class IdentityWithAdditionalClaimsProfileService : IProfileService {
        private readonly IUserClaimsPrincipalFactory<User> _claimsFactory;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole<long>> _roleManager;

        public IdentityWithAdditionalClaimsProfileService(UserManager<User> userManager, IUserClaimsPrincipalFactory<User> claimsFactory, RoleManager<IdentityRole<long>> roleManager) {
            _userManager = userManager;
            _claimsFactory = claimsFactory;
            _roleManager = roleManager;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context) {
            var sub = context.Subject.Claims.Where(f => f.Type == "sub").FirstOrDefault().Value;
            var user = await _userManager.FindByIdAsync(sub);
            var principal = await _claimsFactory.CreateAsync(user);

            var claims = principal.Claims.ToList();
            claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();

            var roles = await _userManager.GetRolesAsync(user);
            foreach (var role in roles) {
                claims.Add(new Claim(JwtClaimTypes.Role, role));


                var foundRole =  _roleManager.Roles.Where(f => f.Name == role).FirstOrDefault();
                var roleClaims = await _roleManager.GetClaimsAsync(foundRole);
                foreach(var roleClaim in roleClaims) {
                    claims.Add(new Claim(roleClaim.Type, roleClaim.Value));
                }
            }
            
            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context) {
            var sub = context.Subject.Claims.Where(f => f.Type == "sub").FirstOrDefault().Value;
            var user = await _userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }
}
