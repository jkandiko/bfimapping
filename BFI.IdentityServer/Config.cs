﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.IdentityServer
{
    public class Config {
        public static IEnumerable<ApiResource> GetApiResources() {
            return new List<ApiResource> {
                new ApiResource("BFI_API_V1", "Bike Fixation - Public Mapping V1") { UserClaims = { JwtClaimTypes.Role } }
            };
        }

        public static IEnumerable<Client> GetClients() {
            return new List<Client> {
                new Client {
                    ClientId = "external.Clients",
                    ClientName = "External Clients",
                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets = { new Secret("bfi-webapp-secret".Sha256()) },

                    // scopes that client has access to
                    AllowedScopes = { "BFI_API_V1" }
                },
                new Client {
                    ClientId = "internal.MVCClient",
                    ClientName = "BFI Mapping MVC Client",

                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,

                    AlwaysSendClientClaims = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowOfflineAccess = true,
                    UpdateAccessTokenClaimsOnRefresh = true,
                    RequireConsent = false,

                    ClientSecrets = {new Secret("bfi-webapp-secret".Sha256()) },

                    RedirectUris           = { "http://localhost:5002/signin-oidc" },
                    PostLogoutRedirectUris = { "http://localhost:5002/signout-callback-oidc" },
                    FrontChannelLogoutUri = "http://localhost:5000/connect/endsession",

                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "BFI_API_V1",
                    },
                },

                new Client {
                    ClientId = "internal.AdminConsoleClient",
                    ClientName = "AdminConsole Client",

                    ClientSecrets = { new Secret("adminconsole-secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    AllowedScopes = { "BFI_API_V1" },
                }
            };
        }

        // scopes define the resources in your system
        public static IEnumerable<IdentityResource> GetIdentityResources() {
            return new List<IdentityResource> {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }
    }
}
