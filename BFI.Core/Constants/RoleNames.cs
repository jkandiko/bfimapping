﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Core.Constants {
    public static class RoleNames {
        public const string BFIAdministrator = "BFI Administrator";
        public const string CompanyAdministrator = "Company Administrator";
        public const string CompanyUser = "Company User";
    }
}
