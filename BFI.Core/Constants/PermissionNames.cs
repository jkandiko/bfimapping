﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Core.Constants {
    public static class PermissionNames {
        public const string CompaniesCreate = "companies.create";
        public const string CompaniesUpdate = "companies.edit";
        public const string CompaniesView = "companies.view";
        public const string CompaniesDelete = "companies.delete";

        public const string LocationsCreate = "locations.create";
        public const string LocationsUpdate = "locations.edit";
        public const string LocationsView = "locations.view";
        public const string LocationsDelete = "locations.delete";

        public const string RegionsCreate = "regions.create";
        public const string RegionsUpdate = "regions.edit";
        public const string RegionsView = "regions.view";
        public const string RegionsDelete = "regions.delete";
    }
}
