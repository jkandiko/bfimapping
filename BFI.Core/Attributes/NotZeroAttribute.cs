﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BFI.Core.Attributes {
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    sealed public class NotZeroAttribute : ValidationAttribute {
        public override bool IsValid(object value) {
            var valu = (decimal)value;
            return valu != 0;
        }
    }
}
