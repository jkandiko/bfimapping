﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BFI.Core.Security {
    public class PermissionRequirementHandler : AuthorizationHandler<PermissionRequirement> {
        public IConfiguration Configuration { get; set; }

        public PermissionRequirementHandler(IConfiguration config) {
            Configuration = config;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement) {
            var hasRole = context.User.HasClaim(c =>
                c.Type == "permission" &&
                c.Issuer == Configuration["IdentityServerAddress"] &&
                c.Value == requirement.PermissionName);

            if (hasRole) {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}