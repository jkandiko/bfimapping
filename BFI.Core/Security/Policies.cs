﻿using BFI.Core.Constants;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Core.Security
{
    public static class Policies {
        public static Dictionary<string, System.Action<AuthorizationPolicyBuilder>> Build() {
            var ret = new Dictionary<string, System.Action<AuthorizationPolicyBuilder>>();

            ret.Add(PolicyNames.BFIAdmin, policy => policy.Requirements.Add(new RoleRequirement(RoleNames.BFIAdministrator)));
            ret.Add(PolicyNames.CompanyAdmin, policy => policy.Requirements.Add(new RoleRequirement(RoleNames.CompanyAdministrator)));
            ret.Add(PolicyNames.CompanyUser, policy => policy.Requirements.Add(new RoleRequirement(RoleNames.CompanyUser)));

            ret.Add(PermissionNames.CompaniesCreate, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.CompaniesCreate)));
            ret.Add(PermissionNames.CompaniesUpdate, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.CompaniesUpdate)));
            ret.Add(PermissionNames.CompaniesView, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.CompaniesView)));
            ret.Add(PermissionNames.CompaniesDelete, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.CompaniesDelete)));

            ret.Add(PermissionNames.LocationsCreate, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.LocationsCreate)));
            ret.Add(PermissionNames.LocationsUpdate, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.LocationsUpdate)));
            ret.Add(PermissionNames.LocationsView, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.LocationsView)));
            ret.Add(PermissionNames.LocationsDelete, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.LocationsDelete)));

            ret.Add(PermissionNames.RegionsCreate, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.RegionsCreate)));
            ret.Add(PermissionNames.RegionsUpdate, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.RegionsUpdate)));
            ret.Add(PermissionNames.RegionsView, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.RegionsView)));
            ret.Add(PermissionNames.RegionsDelete, policy => policy.Requirements.Add(new PermissionRequirement(PermissionNames.RegionsDelete)));

            return ret;
        }
    }
}
