﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Core.Security {
   public class PermissionRequirement : IAuthorizationRequirement {
        public string PermissionName { get; private set; }

        public PermissionRequirement(string permissionName) {
            PermissionName = permissionName;
        }
    }
}