﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace BFI.Core.Security {
    public class RoleRequirementHandler : AuthorizationHandler<RoleRequirement> {
        public IConfiguration Configuration { get; set; }

        public RoleRequirementHandler(IConfiguration config) {
            Configuration = config;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleRequirement requirement) {
            var hasRole = context.User.HasClaim(c =>
                c.Type == "role" &&
                c.Issuer == Configuration["IdentityServerAddress"] &&
                c.Value == requirement.RoleName);

            if (hasRole) {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
