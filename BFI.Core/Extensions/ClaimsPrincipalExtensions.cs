﻿using System.Linq;
using System.Security.Claims;

namespace BFI.Core.Extensions {
    public static class ClaimsPrincipalExtensions {

        public static string Name(this ClaimsPrincipal ctx) {
            return ctx.Claims.Where(f => f.Type == "name").FirstOrDefault()?.Value;
        }

        public static bool HasRole(this ClaimsPrincipal ctx, string name) {
            var hasRole = ctx.HasClaim(c =>
                c.Type == "role" &&
                c.Value == name);
            
            return hasRole;
        }

        public static bool HasPermission(this ClaimsPrincipal ctx, string name) {
            var hasRole = ctx.HasClaim(c =>
                c.Type == "permission" &&
                c.Value == name);

            return hasRole;
        }

        public static string ClientId(this ClaimsPrincipal ctx) {
            return ctx.Claims.Where(f => f.Type == "client_id").FirstOrDefault()?.Value;
        }
    }
}