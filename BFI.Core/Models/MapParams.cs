﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BFI.Core.Models {
    public class MapParams {
        public decimal SouthwestLatitude { get; set; }
        public decimal SouthwestLongitude { get; set; }
        public decimal NortheastLatitude { get; set; }
        public decimal NortheastLongitude { get; set; }
        public string Uri { get; set; }
    }
}
