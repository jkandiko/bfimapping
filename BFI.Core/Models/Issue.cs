﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BFI.Core.Models {
    public class Issue {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
